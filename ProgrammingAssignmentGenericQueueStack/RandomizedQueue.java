import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class RandomizedQueue<Item> implements Iterable<Item>{

    private Item[] s;
    private int N;
    private Deque<Integer> vacantIndex;
    // construct an empty randomized queue
    public RandomizedQueue() {
        vacantIndex = new Deque<>();
        s = (Item[]) new Object[1];
    }

    private Item[] return_array_copy() {
        Item[] copied_array = (Item[]) new Object[s.length];
        System.arraycopy(s, 0, copied_array, 0, s.length);
        return copied_array;
    }

    // is the randomized queue empty?
    public boolean isEmpty() {
        return N == 0;
    }

    private void resize(int capacity) {
        Item[] copy = (Item[]) new Object[capacity];
        for (int i = 0; i < N; i++) {
            copy[i] = s[i];
        }
        s = copy;

    }

    private void bind(int removed, Item[] q) {
        if (q.length == 1) {
            --N;
            q[0] = null;
        } else {
            for (int i = removed; i < q.length - 1; i++) {
                q[i] = q[i + 1];
            }
            q[--N] = null;
        }
    }

    // return the number of items on the randomized queue
    public int size() {
        return N;
    }

    // add the item
    public void enqueue(Item item) {
//        System.out.println(String.format("Enqueueing %s", item));

        if (item == null) throw new IllegalArgumentException("Cannot add null to queue");
//        if (!vacantIndex.isEmpty())
//        {
//            s[vacantIndex.removeFirst()] = item;
//        }
        if (N == s.length) resize(2 * s.length);

        s[N++] = item;
    }

    // remove and return a random item
    public Item dequeue() {

        if (N == 0) throw new NoSuchElementException("Queue is empty");

        int index = StdRandom.uniform(0, N);
        Item toReturn = s[index];
//        vacantIndex.addFirst(index);
        // no need to bind just move last element to removed element
        bind(index, this.s);
        if (N > 0 && N == s.length / 4) resize(s.length / 2);

        return toReturn;
    }

    // return a random item (but do not remove it)
    public Item sample() {
        if (N == 0) throw new NoSuchElementException("Queue is empty");

        return s[StdRandom.uniform(0, N)];
    }

    // return an independent iterator over items in random order
    public Iterator<Item> iterator() {
        return new RandomizedQueueIterator(this);
    }

    private class RandomizedQueueIterator implements Iterator<Item> {
        //        private int current;
        private final RandomizedQueue<Item> que;

        private RandomizedQueueIterator(RandomizedQueue<Item> q) {
            que = new RandomizedQueue<>();
            que.s = q.return_array_copy();
            que.N = q.N;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public boolean hasNext() {
            return que.N != 0;
        }

        public Item next() {
            if (que == null) {
                throw new NoSuchElementException("Queue is empty");
            } else {
                return que.dequeue();
            }
        }
    }

    // unit testing (required)
    public static void main(String[] args){
        RandomizedQueue<Integer> z = new RandomizedQueue<>();
//        System.out.println(z.dequeue());
//        z.enqueue(4);
//        z.enqueue(6);
//        z.enqueue(10);
//        z.enqueue(9);
//        System.out.println("Added");
//
//        Iterator<Integer> iter =  z.iterator();
//
//        System.out.println(iter.next());
//        System.out.println(iter.next());
//        System.out.println(iter.next());
//        System.out.println(iter.next());
//        System.out.println(iter.hasNext());
//
//        for (Integer x: z) {
//            System.out.println(x);
//        }
//
//
////        System.out.println(z.sample());
//        System.out.println(z.dequeue());
//        System.out.println(z.dequeue());
//        System.out.println(z.dequeue());
//        System.out.println(z.dequeue());
        z.enqueue(12);
        System.out.println(z.dequeue());
        System.out.println(z.size());
        z.enqueue(13);
        System.out.println(z.dequeue());
//        System.out.println(z.dequeue());
        System.out.println("Hmm");




    }
}
