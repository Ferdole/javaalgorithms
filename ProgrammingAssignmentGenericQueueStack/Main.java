import edu.princeton.cs.algs4.StdIn;

import java.util.NoSuchElementException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
//        System.out.println("Hello World");
//        Scanner s = new Scanner(System.in);
        System.out.println(args.length);
        Deque<String> in = new Deque<>();
        boolean flag = true;
        while (flag) {
            try {
                in.addFirst(StdIn.readString());
            }
            catch (NoSuchElementException e) {
                flag = false;
            }
        }

        for (String x: in) {

            System.out.println(x);
        }
    }
}
