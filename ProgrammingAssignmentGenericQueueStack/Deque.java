import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {

    // should keep track of first element added and last element. A pointer to first and last object
    private Node first;
    private Node last;
    private int size = 0;

    private class Entry {
        Node head;
    }

    private class Node {
        Node left = null;
        Node right = null;
        Item item;
    }

    // construct an empty deque
    public Deque() {
        // last and first point to themselves
//        first = new Entry();
//        last = new Entry();
//        first.head = last.head;
    }

    // is the deque empty?
    public boolean isEmpty() {
//        System.out.println("Check if Empty");
        return size == 0;
    }

    // return the number of items on the deque
    public int size() {
        return size;
    }

    // add the item to the front
    public void addFirst(Item item) {
//        System.out.println("addFirst");
        if (item == null) {
            throw new IllegalArgumentException("Cannot be called with null argument");
        }

        // first item added needs to be accessible from end and start
        if (size == 0) {
            first = new Node();
            first.item = item;
            last = first;
            size++;
        } else { //if (size == 1) {
            Node oldFirst = first;
            first = new Node();
            first.item = item;
            // first will be second now. Its left Node reference should point to the newly added one
            oldFirst.left = first;
            first.right = oldFirst;
            size += 1;
        }


    }

    // add the item to the back
    public void addLast(Item item) {
//        System.out.println("addLast");
        if (item == null) {
            throw new IllegalArgumentException("Cannot be called with null argument");
        }

        if (size == 0) {
            last = new Node();
            last.item = item;
            first = last;
            size++;
        } else {
            Node oldLast = last;
            last = new Node();
            last.item = item;
            oldLast.right = last;
            last.left = oldLast;
            size += 1;
        }
    }

    // remove and return the item from the front
    public Item removeFirst() {
        if (this.isEmpty()) {
            throw new NoSuchElementException("The Queue is Empty");
        } else if (size > 1) {
            Item toReturn = first.item;
            first = first.right;
            first.left = null;
            size -= 1;
            return toReturn;
        } else if (size == 1) {
            Item toReturn = first.item;
            first = last = null;
            size -= 1;
            return toReturn;
        } else {
            Item toReturn = first.item;
            last = first = null;
            size--;
            return toReturn;
        }
    }

    // remove and return the item from the back
    public Item removeLast() {
        if (this.isEmpty()) {
            throw new NoSuchElementException("The Queue is Empty");
        } else if (size >= 2) {
            Item toReturn = last.item;
            last = last.left;
            last.right = null;
            // last.right should point to null.
            size--;
            return toReturn;
        } else if (size == 1) {
            Item toReturn = first.item;
            first = last = null;
            size -= 1;
            return toReturn;
        } else {
            Item toReturn = last.item;
            last = first = null;
            size--;
            return toReturn;
        }
    }

    // return an iterator over items in order from front to back
    public Iterator<Item> iterator() {
        return new ListIterator();
    }

    private class ListIterator implements Iterator<Item> {
        private Node current = first;

        public boolean hasNext() {
            return current != null;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public Item next() {
            if (current == null) {
                throw new NoSuchElementException("Queue is empty");
            } else {
                Item item = current.item;
                current = current.right;
                return item;
            }
        }
    }

    // unit testing (required)
    public static void main(String[] args) {
        System.out.println("Hello World");
        Deque<Integer> de1 = new Deque<>();
        System.out.println(String.format("%s  \nSize: %d", de1, de1.size()));
        System.out.println(de1.isEmpty());
        de1.addFirst(14);
        de1.removeFirst();
        de1.addLast(29);
//        System.out.println(String.format("Removing last: %s \nSize: %d", de1.removeLast(), de1.size()));
        de1.addFirst(19);
        System.out.println(String.format("Removing last: %s \nSize: %d", de1.removeLast(), de1.size()));
        de1.addFirst(21);
        System.out.println(String.format("Removing last: %s \nSize: %d", de1.removeLast(), de1.size()));
        de1.addLast(39);
        System.out.println(String.format("Size: %d", de1.size()));
        System.out.println(String.format("Removing first: %s \nSize: %d", de1.removeFirst(), de1.size()));
        System.out.println(String.format("Removing last: %s \nSize: %d", de1.removeLast(), de1.size()));
        de1.addLast(50);
        de1.addLast(51);
        de1.addLast(52);

//        Iterator z = de1.iterator();

//        for (Integer x: de1
//             ) {
//            System.out.println(x);
//        }
//
//        }

    }
}
