import edu.princeton.cs.algs4.StdIn;

public class Permutation {
    public static void main(String[] args) {
        final int permutations = Integer.parseInt(args[0]);
//        System.out.println(args.length);
//        Deque<String> deque = new Deque<>();
        final RandomizedQueue<String> randQueue = new RandomizedQueue<>();
//        int how = 0;
        while (!StdIn.isEmpty()) {
//            System.out.println(how++);
            randQueue.enqueue(StdIn.readString());
        }
//        System.out.println(String.format("Dequeueing %d items", permutations));
        for (int i = 0; i < permutations; i++) {
            System.out.println(randQueue.dequeue());
        }

    }
}
