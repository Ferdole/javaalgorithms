package List;

public class EmptyListInterface<T> extends ListInterface {

    private ListInterface<T> head;
    private ListInterface<T> next;

    @Override
    public boolean isEmpty() {
        return true;
    }

    @Override
    public ListInterface getHead() {
        return null;
    }

    @Override
    public ListInterface getTail() {
        return next;
    }
}
