package List;

public class List {

    public List(int val) {
        this.val = val;
    }

    int val;
    List next;

    public static void main(String[] args) {

        List one = new List(1);
        List two = new List(2);

        one.next = two;
        List element = one;

        while (element.next != null){
            element = element.next;
        }

        System.out.println(String.format("Element val is %s", element.val));

    }

}
