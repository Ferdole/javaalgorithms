package List;

public class LinkedStackOfString {

    private Node first = null;
    private int size = 0;
    private class  Node {
        String item;
        Node next;
    }

    public boolean isEmpty() {
        return first == null;
    }

    public int length() {
        return this.size;
    }

    public void incrementSize() {
        this.size ++;
    }

    public void decrementSize() {
        this.size --;
    }


    public void push(String item) {
        Node oldFirst = first;
        first = new Node();
        first.item = item;
        first.next = oldFirst;
        incrementSize();
    }

    public String pop()
    {
        String toPopString = first.item;
        first = first.next;
        decrementSize();
        return toPopString;
    }


    public static void main(String[] args) {

        LinkedStackOfString testLinked = new LinkedStackOfString();
        testLinked.push("dorel");
        testLinked.push("carpeta");

        System.out.println("Dorel".length());

        System.out.println(testLinked.length());
        System.out.println("Pop: " + testLinked.pop());
        System.out.println(testLinked.length());

    }

}
