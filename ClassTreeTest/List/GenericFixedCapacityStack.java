package List;

public class GenericFixedCapacityStack<Item> {

    private Item[] s;
    private int N = 0;

    public GenericFixedCapacityStack(int capacity) {
        //have to cast item to Object type array
        s = (Item[]) new Object[capacity];
    }

    public boolean isEmpty() {
        return N == 0;
    }

    public void push(Item item) {
        s[N++] = item;
    }

    public Item pop()
    {
        return s[--N];
    }

    // resize upper bound to double when full and reduce size when stack is less than 1/4 full
    private void resize(int capacity){
        Item[] copy = (Item[]) new Object[capacity];
        for (int i = 0; i <= N; i++){
            copy[i] = s[i];
        }
        s = copy;
    }

}
