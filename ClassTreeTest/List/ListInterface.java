package List;

public abstract class ListInterface<T> {
    // interfaces can only have constants

    abstract boolean isEmpty();
    abstract public ListInterface<T> getHead();
    abstract public ListInterface<T> getTail();
}
