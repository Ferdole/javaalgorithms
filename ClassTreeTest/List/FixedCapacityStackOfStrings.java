package List;

public class FixedCapacityStackOfStrings {

    private String[] s;
    private int N = 0;

    public FixedCapacityStackOfStrings(int capacity) {
        s = new String[capacity];
    }

    public boolean isEmpty() {
        return N == 0;
    }

    public void push(String item) {
        s[N++] = item;
    }

    public String pop() {
        return s[--N];
    }

    public static void main(String[] args) {

        int ar[] = new int[20];
        ar[2] = 3;
        int int1 = 4;
        int1--;
        System.out.println(ar.length);
    }

}
