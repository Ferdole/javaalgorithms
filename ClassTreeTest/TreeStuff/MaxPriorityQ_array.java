package TreeStuff;

import java.util.Arrays;

public class MaxPriorityQ_array<Key extends Comparable<Key>> {
    private int N;
    private Key[] pq;
    // go down in level with 2k / 2k + 1
    // parent of note k at k/2

    public MaxPriorityQ_array(int capacity){
        this.pq = (Key[]) new Comparable[capacity];
        this.N = 0;
    }

    public void insert(Key key){
//        pq[N++] = key;
        pq[++N] = key;
        swim(N);

    }

    // bring element to a higher rank if necessary
    private void swim(int k){
        while ( k > 1 && less(k/2, k)) {
            // parent at k/2
            exchange(k/2, k);

            k = k/2;
        }
    }

    private void sink(int k){
        while ( 2*k <= N) {

            // check which child is bigger
            int j = 2*k;
            if ( j < N && less(j, 2*k + 1) ) j++;
            if (!less(k, j)) break;
            exchange(k, j);
            k=j;
        }
    }

    public Key delMax() {
        Key max = pq[1];
        // exchange last Element from Array with root and sink it
        exchange(1, N--);
        sink(1);
        pq[N+1] = null;
        return max;
    }

    private void exchange(int i, int j){
        Key aux = pq[i];
        pq[i] = pq[j];
        pq[j] = aux;
    }

    private boolean less(int i, int j){
        return pq[i].compareTo(pq[j]) < 0;
    }

    @Override
    public String toString() {
        return "MaxPriorityQ_array{" +
                "N=" + N +
                ", pq=" + Arrays.toString(pq) +
                '}';
    }

    public static void main(String[] args) {
        MaxPriorityQ_array<Integer> maxPQ = new MaxPriorityQ_array<>(20);

        maxPQ.insert(6);
        maxPQ.insert(3);
        maxPQ.insert(1);
        maxPQ.insert(11);
        maxPQ.insert(4);
        maxPQ.insert(9);
        maxPQ.insert(10);
        System.out.println(maxPQ);
        System.out.println(maxPQ.delMax());
        System.out.println(maxPQ);
    }
}
