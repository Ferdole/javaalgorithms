package puzzle_8;

import java.util.LinkedList;

public class Board{

    private final int[][] tiles;
    private int blankRow;
    private int blankColumn;

    private boolean isBlankTile(int[][] tiles, int i, int j) {
        return tiles[i][j] == 0;
    }

    private int[][] copyAndBlankTile(int[][] tiles) {

        int[][] clonedTiles = new int[tiles.length][tiles.length];

        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles.length; j++) {
                if (isBlankTile(tiles, i, j)) {
                    this.blankRow = i;
                    this.blankColumn = j;

                }

                clonedTiles[i][j] = tiles[i][j];
            }
        }
        return clonedTiles;
    }

    // create a board from an n-by-n array of tiles,
    // where tiles[row][col] = tile at (row, col)
    public Board(int[][] tiles){

        if (tiles == null) throw new IllegalArgumentException("Tiles are Null");

        this.tiles = this.copyAndBlankTile(tiles);
    }

//     string representation of this board
    public String toString() {
        String board = String.format("%d", tiles.length);
        String row = "";
//        board = String.format("%s \nPriority: %d", board, this.manhattan());
        for (int i = 0; i < tiles.length; i++) {
            board = board + "\n";
            for (int j = 0; j < tiles.length; j++) {
                board = String.format("%s %d", board, tiles[i][j]);
            }

        }
        return board;
    }

    // board dimension n
    public int dimension() { return this.tiles.length; }
//
    // number of tiles out of place
    public int hamming(){

        int hamming = 0;

        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles.length; j++) {

                // tile should be at i * tiles.length + j + 1
                if (tiles[i][j] == 0) continue;
                else if (i * tiles.length + j + 1 != tiles[i][j]) hamming++;

            }
        }
        return hamming;
    }

    // sum of Manhattan distances between tiles and goal
    public int manhattan() {

        int manhattan_sum = 0;
        int target_row = 0;
        int target_column = 0;

        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles.length; j++) {

                if (tiles[i][j] == 0) continue;

                target_row = (tiles[i][j] - 1) / tiles.length;

                if ( target_row > i) {  // tiles should be moved on a higher row
//                    System.out.println("On wrong row: " + tiles[i][j] );
                    manhattan_sum += target_row - i;
                } else if (target_row < i) {
//                    System.out.println("On wrong row: " + tiles[i][j] );
                    manhattan_sum += i - target_row;
                }

                target_column = tiles[i][j] - target_row * tiles.length - 1;

                if (target_column > j) manhattan_sum += target_column - j;
                else if (target_column < j) manhattan_sum += j - target_column;

            }

        }
        return manhattan_sum;
    }

    // is this board the goal board?
    public boolean isGoal(){

        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles.length; j++) {

                // TODO rewrite this since it will always yield true if last tile == 0
                if(i == tiles.length - 1 && j == tiles.length - 1) return tiles[i][j] == 0;
                else if ( tiles[i][j] !=  i * tiles.length + j + 1 ) return false;
            }
        }
        return false;
    }

    // does this board equal y?
    public boolean equals(Object y) {
//        puzzle_8.Board b = (puzzle_8.Board) y;

        if (y == this) return true;

        if (y == null) return false;

        if (y instanceof Board) {
            final Board b = (Board) y;
            if (b.dimension() != this.dimension())
                return false;
            else {
                for (int i = 0; i < tiles.length; i++) {
                    for (int j = 0; j < tiles.length; j++) {
                        if (tiles[i][j] != b.tiles[i][j]) {
                            return false;
                        }
                    }
                }
            }
            return true;
        } else return false;
    }

    // all neighboring boards
    public Iterable<Board> neighbors(){

        LinkedList<Board> neighbors = new LinkedList<>();

        Board neighbour;

        // defining limits. reverse blank tile with neighboring tiles
        // generate new board at each step
        // look up
        if(blankRow > 0) {
            int[][] newTiles = copyAndBlankTile(tiles);
            exchange(newTiles, blankRow, blankColumn, blankRow - 1, blankColumn);
            neighbour = new Board(newTiles);
            neighbors.add(neighbour);
        }
        // look down
        if(blankRow + 1 < tiles.length) {
            int[][] newTiles = copyAndBlankTile(tiles);
            exchange(newTiles, blankRow, blankColumn, blankRow + 1, blankColumn);
            neighbour = new Board(newTiles);
            neighbors.add(neighbour);
        }
        // look left
        if(blankColumn > 0) {
            int[][] newTiles = copyAndBlankTile(tiles);
            exchange(newTiles, blankRow, blankColumn, blankRow, blankColumn - 1);
            neighbour = new Board(newTiles);
            neighbors.add(neighbour);
        }
        // look right
        if(blankColumn + 1 < tiles.length) {
            int[][] newTiles = copyAndBlankTile(tiles);
            exchange(newTiles, blankRow, blankColumn, blankRow, blankColumn + 1);
            neighbour = new Board(newTiles);
            neighbors.add(neighbour);
        }

        return neighbors;

    }

    private void exchange(int[][] clonedTile, int si, int sj, int di, int dj){

        int aux = clonedTile[di][dj];
        clonedTile[di][dj] = clonedTile[si][sj];
        clonedTile[si][sj] = aux;

    }

    // a board that is obtained by exchanging any pair of tiles
    public Board twin(){

        int[][] newTiles = copyAndBlankTile(tiles);
        // change row if we are on blank row and switch from there
        if (blankRow == 0){
            exchange(newTiles, blankRow + 1, 0, blankRow + 1, 1);
        } else if (blankRow == tiles.length - 1){
            exchange(newTiles, blankRow - 1, 0, blankRow - 1, 1);
        }
        else {
            exchange(newTiles, blankRow - 1, 0, blankRow - 1, 1);
        }
        return new Board(newTiles);
    }

    // unit testing (not graded)
    public static void main(String[] args){
        int[][] f = new int[3][3];
        int[][] g = new int[3][3];
        for (int i = 0; i < f.length; i++) {
            for (int j = 0; j < f.length; j++) {
                f[i][j] = i * f.length + j + 1;
            }
        }

        for (int i = 0; i < g.length; i++) {
            for (int j = 0; j < g.length; j++) {
                g[i][j] = i * g.length + j + 1;
            }
        }


//        System.out.println("Is goal: " + b1.isGoal());
        g[1][1] = 6;
        g[1][2] = 5;
        g[2][2] = 0;
//        f[0][3] = 9;
//        f[3][1] = 3;
//        f[0][2] = 14;
        f[0][0] = 0;
        f[0][1] = 2;
        f[0][2] = 5;
        f[1][0] = 3;
        f[1][1] = 1;
        f[1][2] = 8;
        f[2][0] = 4;
        f[2][1] = 7;
        f[2][2] = 6;
        Board b = new Board(f);
        int cnt = 0;
        System.out.println("puzzle_8.Board: " + b);
        System.out.println("\nTwinBoard: " + b.twin());
        for (Board neigh:b.neighbors()) {
            cnt ++;
            System.out.println(String.format("Neighbour %d \n %s", cnt, neigh));
        }

        Board b2 = new Board(g);
        cnt = 0;

        System.out.println("\nb2 TwinBoard: " + b2.twin() + "\nfrom puzzle_8.Board: " + b2);

        for (Board neigh:b2.neighbors()) {
            cnt ++;
            System.out.println(String.format("Neighbour %d \n %s", cnt, neigh));
        }

        System.out.println("b is goal: " + b.isGoal() + "\n" + b);
        System.out.println("b2 is goal: " + b2.isGoal() + "\n" + b2);

        //        System.out.println(f.length);
        System.out.println("Hamming: " + b.hamming());
        System.out.println("Manhattan: " + b.manhattan());

        Iterable<Board> neighborBoards = b.neighbors();

        for (Board z: neighborBoards) {
            System.out.println(z);
        }

//        System.out.println(new puzzle_8.Board(neighborBoards.next()));
        System.out.println(b);
//        System.out.println(new puzzle_8.Board(neighborBoards.next()));
//        System.out.println(new puzzle_8.Board(neighborBoards.next()));

    }

}