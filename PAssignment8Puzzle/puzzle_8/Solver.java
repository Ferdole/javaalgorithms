package puzzle_8;

import edu.princeton.cs.algs4.MinPQ;

import java.util.LinkedList;


public class Solver{

    private MinPQ<SearchNode> pq;
    private LinkedList<Board> solution;
    private final int minMoves;
    private SearchNode sol;
    private int isSolvable;

    private class SearchNode implements Comparable<SearchNode>
    {
        private final SearchNode parentNode;
        private final Board b;
        private final int moves;
        private final int priority;

        public int getMoves() {
            return moves;
        }

        public SearchNode getParentNode() {
            return parentNode;
        }

        public Board getB() {
            return b;
        }

        // implement comparator
        SearchNode(Board b, SearchNode p, int moves) {
            this.parentNode = p;
            this.b = b;
            this.priority = b.manhattan() + moves;
            this.moves = moves;
        }

        @Override
        public int compareTo(SearchNode o) {
            return this.priority - o.priority;
        }

    }

    // find a solution to the initial board (using the A* algorithm)
    public Solver(Board b){
        // TODO refactor sometime and write tests

        if (b == null) throw new IllegalArgumentException("Null puzzle_8.Board");

        this.sol = new SearchNode(b, null, 0);

        // make a new searchNode from the twin
        SearchNode solTwin = new SearchNode(b.twin(), null, 0);

        MinPQ<SearchNode> minPQ = new MinPQ<>();
        MinPQ<SearchNode> minPQTwin = new MinPQ<>();

        while (true) {

            if (this.sol.b.isGoal()) {
                this.minMoves = this.sol.moves;
                break;
            }
            else if (solTwin.b.isGoal()){
                this.minMoves = -1;
                break;
            }

            // twin moves and normal board moves are the same
            int moves = this.sol.getMoves();
//            puzzle_8.Board prev = moves > 0 ? this.sol.getParentNode().getB() : null;
//            puzzle_8.Board prevTwin = moves > 0 ? solTwin.getParentNode().getB() : null;


            for (Board neighbor : this.sol.b.neighbors()) {
//                if (prev != null && neighbor.equals(this.sol.getParentNode().getB())) {
//                    continue;
//                }
                if (moves == 0) {
                    SearchNode z = new SearchNode(neighbor, this.sol, this.sol.moves + 1);
                    minPQ.insert(z);
                } else if (!neighbor.equals(this.sol.getParentNode().getB())) {
                    SearchNode z = new SearchNode(neighbor, this.sol, this.sol.moves + 1);
                    minPQ.insert(z);
                }
            }

            for (Board neighbor : solTwin.b.neighbors()) {
//                if (prevTwin != null && neighbor.equals(solTwin.getParentNode().getB())) {
//                    continue;
//                }
                if (moves == 0) {
                    SearchNode z = new SearchNode(neighbor, solTwin, solTwin.moves + 1);
                    minPQTwin.insert(z);
                } else if (!neighbor.equals(solTwin.getParentNode().getB())) {
                    SearchNode z = new SearchNode(neighbor, solTwin, solTwin.moves + 1);
                   minPQTwin.insert(z);
                }
            }

            this.sol = minPQ.delMin();
            solTwin = minPQTwin.delMin();
        }


    }

    // is the initial board solvable? (see below)
    public boolean isSolvable() {
        return minMoves >= 0;
    }

    // min number of moves to solve initial board; -1 if unsolvable
    public int moves(){
        return this.minMoves;
    }

    // sequence of boards in a shortest solution; null if unsolvable
    public Iterable<Board> solution(){

        SearchNode sol = this.sol;
        LinkedList<Board> solution = new LinkedList<>();

        if (this.minMoves == -1) {
            return null;
        } else {
            while (sol != null) {
                solution.addFirst(sol.b);
                sol = sol.parentNode;
            }
        }

        return solution;
    }

    // test client (see below)
    public static void main(String[] args){
        int[][] f = new int[3][3];

        for (int i = 0; i < f.length; i++) {
            for (int j = 0; j < f.length; j++) {
                f[i][j] = i * f.length + j + 1;
            }
        }
        // test1
//        f[0][0] = 6;
//        f[0][1] = 2;
//        f[0][2] = 5;
//        f[1][0] = 3;
//        f[1][1] = 1;
//        f[1][2] = 8;
//        f[2][0] = 4;
//        f[2][1] = 7;
//        f[2][2] = 0;
        // test2
//        f[0][0] = 1;
//        f[0][1] = 2;
//        f[0][2] = 3;
//        f[1][0] = 0;
//        f[1][1] = 7;
//        f[1][2] = 6;
//        f[2][0] = 5;
//        f[2][1] = 4;
//        f[2][2] = 8;
        // test 3
        f[0][0] = 2;
        f[0][1] = 1;
        f[0][2] = 3;
        f[1][0] = 4;
        f[1][1] = 0;
        f[1][2] = 5;
        f[2][0] = 7;
        f[2][1] = 6;
        f[2][2] = 8;
//        puzzle_8.Board b1 = new puzzle_8.Board(f);
//        System.out.println("Is goal: " + b1.isGoal());
////        f[0][0] = 2;
////        f[0][1] = 1;
////        f[1][2] = 5;
////        f[1][1] = 0;
////        f[2][2] = 6;
//
////        f[2][0] = 4;
////        f[0][3] = 9;
////        f[3][1] = 3;
////        f[0][2] = 14;
//
        Board b = new Board(f);
        System.out.println("Initial puzzle_8.Board: " + b);
//        System.out.println(b);
        Solver sol = new Solver(b);
//        System.out.println("Soltuion: " + sol.sol().b);
//        System.out.println("Moves from goal: " + sol.moves());
        int cnt = 0;
////        System.out.println("Len Solution: " + sol.solution.toArray().length);
        for (Board s: sol.solution()) {
            System.out.println(String.format("Position: %d \n%s", cnt, s));
            cnt ++;
        }

        System.out.println("Is solvable: "  + sol.isSolvable());
        System.out.println("cnt: " + cnt);
//
//        cnt = 0;
//
//        System.out.println("Seconds call to solution");
//        for (puzzle_8.Board s : sol.solution()) {
//            System.out.println(String.format("Position: %d \n%s", cnt, s));
//            cnt++;
//        }
//
//        System.out.println("CNT: " + cnt);

        System.out.println("\n\nNew size 4 array \n");
        int[][] r = new int[4][4];

        for (int i = 0; i < r.length; i++) {
            for (int j = 0; j < r.length; j++) {
                r[i][j] = i * r.length + j + 1;
            }
        }

        r[0][0] = 1;
        r[0][1] = 2;
        r[0][2] = 4;
        r[0][3] = 12;
        r[1][0] = 5;
        r[1][1] = 6;
        r[1][2] = 3;
        r[1][3] = 0;
        r[2][0] = 9;
        r[2][1] = 10;
        r[2][2] = 8;
        r[2][3] = 7;
        r[3][0] = 13;
        r[3][1] = 14;
        r[3][2] = 11;
        r[3][3] = 15;

        Board r1 = new Board(r);
//        System.out.println(b);
        Solver sol2 = new Solver(r1);
//        sol.solution.toArray();

        System.out.println("Solution for puzzle_8.Board: \n" + r1 + "\nIn min Moves: " + sol2.moves());
//        searchNode sol2 = sol2.sol();

//        for (puzzle_8.Board s : sol2.solution()) {
//            System.out.println(s);
//            cnt2++;
//        }

//        System.out.println(b);
////        System.out.println(f.length);
//        System.out.println(b.hamming());
//        System.out.println(b.manhattan());
//
////        System.out.println(b.blank);
//
//        Iterable<puzzle_8.Board> neighborBoards = b.neighbors();
//
//        for (puzzle_8.Board z: neighborBoards) {
//
//            System.out.println(z);
//        }
//
////        System.out.println(new puzzle_8.Board(neighborBoards.next()));
//        System.out.println(b);

    }

}
