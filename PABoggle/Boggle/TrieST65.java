package Boggle;

import edu.princeton.cs.algs4.Queue;

import java.util.ArrayList;

public class TrieST65 {
    private static final int R = 26;        // extended ASCII
    private static final int normalizer = 65; // Reduce each char value to be in between 0-25

    private Node root;                  // root of trie
    private ArrayList<Node> nodeIndex;  // track current position of the Node we are looking at
    private int n;                      // number of keys in trie


    public static class Node {
        private final Node[] next = new Node[R];
        private Object val;
    }

    /**
     * Initializes an empty string symbol table.
     */
    public TrieST65() {
        this.nodeIndex = new ArrayList<>();
    }

    public Node getRoot() {
        return this.root;
    }

    public Object getNodeValue(Node x) {
        return x.val;
    }
    /**
     * Returns the value associated with the given key.
     *
     * @param key the key
     * @return the value associated with the given key if the key is in the symbol table
     * and {@code null} if the key is not in the symbol table
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public Integer get(String key) {
        if (key == null) throw new IllegalArgumentException("argument to get() is null");
        Node x = get(root, key, 0);
        if (x == null) return null;
        return (Integer) x.val;
    }

    /**
     * Does this symbol table contain the given key?
     *
     * @param key the key
     * @return {@code true} if this symbol table contains {@code key} and
     * {@code false} otherwise
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public boolean contains(String key) {
        if (key == null) throw new IllegalArgumentException("argument to contains() is null");
        return get(key) != null;
    }

    private Node get(Node x, String key, int d) {
        if (x == null) return null;
        if (d == key.length()) return x;
        char c = key.charAt(d);
        int index = c - normalizer;
        return get(x.next[index], key, d + 1);
    }


    /**
     * Inserts the key-value pair into the symbol table, overwriting the old value
     * with the new value if the key is already in the symbol table.
     * If the value is {@code null}, this effectively deletes the key from the symbol table.
     *
     * @param key the key
     * @param val the value
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public void put(String key, Integer val) {
        if (key == null) throw new IllegalArgumentException("first argument to put() is null");
        else root = put(root, key, val, 0);
    }

    private Node put(Node x, String key, Integer val, int d) {
        if (x == null) x = new Node();
        if (d == key.length()) {
            if (x.val == null) n++;
            x.val = val;
            return x;
        }
        char c = key.charAt(d);
        int index = c - normalizer;
        x.next[index] = put(x.next[index], key, val, d + 1);
        return x;
    }

    public Iterable<String> keys() {
        return keysWithPrefix("");
    }

    /**
     * Returns all of the keys in the set that start with {@code prefix}.
     *
     * @param prefix the prefix
     * @return all of the keys in the set that start with {@code prefix},
     * as an iterable
     */
    public Iterable<String> keysWithPrefix(String prefix) {
        Queue<String> results = new Queue<String>();
        Node x = get(root, prefix, 0);
        collect(x, new StringBuilder(prefix), results);
        return results;
    }

    public Node getNextPossibleNode(Node x, char c) {
        int index = c - normalizer;
        return x.next[index];
    }

    private void collect(Node x, StringBuilder prefix, Queue<String> results) {
        if (x == null) return;
        if (x.val != null) results.enqueue(prefix.toString());
        for (char c = 0; c < R; c++) {
            prefix.append((char) (c + normalizer));
            collect(x.next[c], prefix, results);
            prefix.deleteCharAt(prefix.length() - 1);
        }
    }

    /**
     * Returns all of the keys in the symbol table that match {@code pattern},
     * where . symbol is treated as a wildcard character.
     *
     * @param pattern the pattern
     * @return all of the keys in the symbol table that match {@code pattern},
     * as an iterable, where . is treated as a wildcard character.
     */
    public Iterable<String> keysThatMatch(String pattern) {
        Queue<String> results = new Queue<String>();
        collect(root, new StringBuilder(), pattern, results);
        return results;
    }

    private void collect(Node x, StringBuilder prefix, String pattern, Queue<String> results) {
        if (x == null) return;
        int d = prefix.length();
        if (d == pattern.length() && x.val != null)
            results.enqueue(prefix.toString());
        if (d == pattern.length())
            return;
        char c = pattern.charAt(d);
        if (c == '.') {
            for (char ch = 0; ch < R; ch++) {
                prefix.append(ch);
                collect(x.next[ch], prefix, pattern, results);
                prefix.deleteCharAt(prefix.length() - 1);
            }
        } else {
            prefix.append(c);
            collect(x.next[c], prefix, pattern, results);
            prefix.deleteCharAt(prefix.length() - 1);
        }
    }

    /**
     * Returns the string in the symbol table that is the longest prefix of {@code query},
     * or {@code null}, if no such string.
     *
     * @param query the query string
     * @return the string in the symbol table that is the longest prefix of {@code query},
     * or {@code null} if no such string
     * @throws IllegalArgumentException if {@code query} is {@code null}
     */
    public String longestPrefixOf(String query) {
        if (query == null) throw new IllegalArgumentException("argument to longestPrefixOf() is null");
        int length = longestPrefixOf(root, query, 0, -1);
        if (length == -1) return null;
        else return query.substring(0, length);
    }

    // returns the length of the longest string key in the subtrie
    // rooted at x that is a prefix of the query string,
    // assuming the first d character match and we have already
    // found a prefix match of given length (-1 if no such match)
    private int longestPrefixOf(Node x, String query, int d, int length) {
        if (x == null) return length;
        if (x.val != null) length = d;
        if (d == query.length()) return length;
        char c = query.charAt(d);
        return longestPrefixOf(x.next[c], query, d + 1, length);
    }

    public static void main(String[] args) {

        TrieST65 st65 = new TrieST65();
//        st65.put("AAST", 1);
//        st65.put("AASTASD", 3);
//        st65.put("AASTASDR", 5);
//        st65.put("AASTAAASD", 7);
//        st65.put("AASTAAASD", 7);
//        st65.put("AASTAAAS", 7);
//        st65.put("AASTAAAG", 7);
        st65.put("A", 4);
        st65.put("B", 3);
        st65.put("QUE", 2);

        Node x = st65.nodeIndex.get(1);
        for (String s: st65.keysWithPrefix("TEST")) {
            System.out.println(s);
        }
        Node nextNode = st65.getNextPossibleNode(st65.root, 'X');
        System.out.println(st65.getNextPossibleNode(st65.root, 'X') == null);

//        System.out.println(st65.getNextPossibleNode(nextNode, 'C') == null);
//        System.out.println(st65.checkPossibleCharAfterNodeIndex('S', 7));
//        System.out.println(st65.checkPossibleCharAfterNodeIndex('G', 7));
//        System.out.println(st65.checkPossibleCharAfterNodeIndex('C', 1));

    }
}
