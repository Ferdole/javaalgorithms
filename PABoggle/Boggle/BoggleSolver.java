package Boggle;

import edu.princeton.cs.algs4.TrieSET;

//import java.io.BufferedReader;
//import java.io.FileReader;

public class BoggleSolver
{

    // Value first letters, Node has value of words if it is valid word
    private TrieST65 dictionaryTrieSymbolTable;
    private int maximumLengthWord;
    private TrieSET validWords;


    private static Integer valueOfString(String s) {

        if (s.length() < 3) return 0;
        else if (s.length() == 3 || s.length() == 4) return 1;
        else if (s.length() == 5) return 2;
        else if (s.length() == 6) return 3;
        else if (s.length() == 7) return 5;
        else return 11;
    }

    private void constructTrieSymbolTable(String[] dictionaryList) {
        this.dictionaryTrieSymbolTable = new TrieST65();

        this.maximumLengthWord = 0;
        for (String s: dictionaryList) {
            if (this.maximumLengthWord < s.length()) this.maximumLengthWord = s.length();

            this.dictionaryTrieSymbolTable.put(s, valueOfString(s));
        }
    }

//     Initializes the data structure using the given array of strings as the dictionary.
//     (You can assume each word in the dictionary contains only the uppercase letters A through Z.)
    public BoggleSolver(String[] dictionary) {
        constructTrieSymbolTable(dictionary);
    }
    // Returns the set of all valid words in the given Boggle board, as an Iterable.
    public Iterable<String> getAllValidWords(BoggleBoard board) {

        this.validWords = new TrieSET();

        int nrows = board.rows();
        int ncol  = board.cols();

        for (int i=0; i < nrows; i++) {
            for (int j=0; j < ncol; j++) {
                TrieST65.Node root = this.dictionaryTrieSymbolTable.getRoot();
                getLetters(board, i, j, "", new boolean[board.rows()][board.cols()], root);
            }
        }
        return this.validWords;
    }

    private void getLetters(BoggleBoard board, int i, int j, String currentPrefix, boolean[][] visitedMatrix, TrieST65.Node currentNode) {

        char letter = board.getLetter(i, j);
        TrieST65.Node nextNode;

        if (letter == 'Q') {
            currentPrefix += "QU";
            TrieST65.Node interimNode = this.dictionaryTrieSymbolTable.getNextPossibleNode(currentNode, 'Q');
            if (interimNode == null) return;
            else {
                nextNode = this.dictionaryTrieSymbolTable.getNextPossibleNode(interimNode, 'U');
            }
        } else {
            currentPrefix += letter;
            nextNode = this.dictionaryTrieSymbolTable.getNextPossibleNode(currentNode, letter);
        }

//        System.out.println("CurrentPrefix: " + currentPrefix);

        if (nextNode == null) return;
        Object value = this.dictionaryTrieSymbolTable.getNodeValue(nextNode);

        if (value != null && currentPrefix.length() > 2) {
//            System.out.println("Found a match on currentPrefix " + currentPrefix + " and key " + currentPrefix);
            this.validWords.add(currentPrefix);
        }

//        if (currentPrefix.length() >= this.maximumLengthWord){
//            // Really needed ?
//            return;
//        }

        visitedMatrix[i][j] = true;

        // E
        if (j + 1 < board.cols() && !visitedMatrix[i][j + 1]) {
            getLetters(board, i, j + 1, currentPrefix, visitedMatrix, nextNode);
        }
        // SE
        if (i + 1 < board.rows() && j + 1 < board.cols() && !visitedMatrix[i + 1][j + 1]) {
            getLetters(board, i + 1, j + 1, currentPrefix, visitedMatrix, nextNode);
        }
        // S
        if (i + 1 < board.rows() && !visitedMatrix[i + 1][j]) {
            getLetters(board, i + 1, j, currentPrefix, visitedMatrix, nextNode);
        }
        // SW
        if (i + 1 < board.rows() && j - 1 >= 0  && !visitedMatrix[i + 1][j - 1]) {
             getLetters(board, i + 1, j - 1, currentPrefix, visitedMatrix, nextNode);
        }
        // W
        if (j - 1 >= 0   && !visitedMatrix[i][j - 1]) {
            getLetters(board, i, j - 1, currentPrefix, visitedMatrix, nextNode);
        }
        // NW
        if (i - 1 >= 0 && j - 1 >= 0  && !visitedMatrix[i - 1][j - 1]) {
            getLetters(board, i - 1, j - 1, currentPrefix, visitedMatrix, nextNode);
        }
        // N
        if (i - 1 >= 0  && !visitedMatrix[i - 1][j]) {
            getLetters(board, i - 1, j, currentPrefix, visitedMatrix, nextNode);
        }
        // NE
        if (i - 1 >= 0 && j + 1 < board.cols()  && !visitedMatrix[i - 1][j + 1]) {
            getLetters(board, i - 1, j + 1, currentPrefix, visitedMatrix, nextNode);
        }

        // when we return un-mark as visited
        visitedMatrix[i][j] = false;
    }

//     Returns the score of the given word if it is in the dictionary, zero otherwise.
//     (You can assume the word contains only the uppercase letters A through Z.)
    public int scoreOf(String word) {
        Integer score = this.dictionaryTrieSymbolTable.get(word);
        if (score == null) return 0;
        else return score;
    }

    public static void main(String[] args) {

        // 'A' = 65
//        System.out.println('Z' - 'A');

//        ArrayList<String> dict = new ArrayList<>();
//        try (BufferedReader br = new BufferedReader(new FileReader("./PABoggle/Boggle/dictionary-algs4.txt"))) {
//            String line;
//            while ((line = br.readLine()) != null) {
//                dict.add(line);
//            }
//        } catch (Exception e){
//            e.printStackTrace();
//        }
//
//        BoggleBoard test = new BoggleBoard("./PABoggle/Boggle/board-q.txt");
//
//        System.out.println(test.getLetter(2, 1));
////
//        BoggleSolver solver = new BoggleSolver(new String[]{"DOREL", "MERGE", "ARC", "ARSE", "AREC", "QUIET",
//                "QUESTIONS", "QUESTION", "QUEST"});
        BoggleSolver solver = new BoggleSolver(new String[]{"ARC", "ARSE", "AREC", "QUEST", "GAUL", "QUE", "AR", "GOATHERD"});
//        BoggleSolver solver = new BoggleSolver(new String[]{"DOREL", "MERGE", "ARC", "ARE", "AREC"});
//        BoggleSolver solver = new BoggleSolver(dict.toArray(new String[0]));
//        Iterable<String> i = solver.getAllValidWords(new BoggleBoard("./PABoggle/Boggle/boggle32x1.txt"));
//        Iterable<String> i = solver.getAllValidWords(new BoggleBoard("./PABoggle/Boggle/board2x2.txt"));

//        Iterable<String> i = solver.getAllValidWords(new BoggleBoard("./PABoggle/Boggle/board-q.txt"));
        Iterable<String> i = solver.getAllValidWords(new BoggleBoard("./PABoggle/Boggle/board-q2x2.txt"));
//        Iterable<String> i = solver.getAllValidWords(new BoggleBoard("./PABoggle/Boggle/board4x4t1.txt"));

        System.out.println(solver.scoreOf("QUE"));
        System.out.println(solver.scoreOf("GOATHERD"));
        System.out.println(solver.scoreOf("GAUL"));
        System.out.println(solver.scoreOf("WOMAN"));
        int acc = 0;
        for (String s: i){
            System.out.println(s);
            acc ++;
        }
        System.out.println("Words: " + acc);
    }
}
