import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

import java.util.Arrays;

public class BruteCollinearPoints {

    private int numberOfSegments = 0;
    private LineSegment[] lines;


    private void resize(int capacity){
        LineSegment[] aux = new LineSegment[capacity];
        for (int i = 0; i < lines.length; i++) {
            aux[i] = lines[i];
        }
        lines = aux;
    }

    private Point[] sortPoints (Point o1, Point o2, Point o3, Point o4){
        Point[] sortedPoints = new Point[4];
        sortedPoints[0] = o1;
        sortedPoints[1] = o2;
        sortedPoints[2] = o3;
        sortedPoints[3] = o4;

        Arrays.sort(sortedPoints);
        return sortedPoints;
    }

//    private Point returnSmall(Point o1, Point o2){
//        int c = o1.compareTo(o2)
//    }
//    private Point returnBig(Point o1, Point o2)

    // finds all line segments containing 4 points
    public BruteCollinearPoints(Point[] points) {

        if (points == null) throw new IllegalArgumentException("Points is null");

        Point[] copied = new Point[points.length];
//        System.arraycopy(points, 0, copied, 0, points.length);
        for (int i = 0; i < points.length; i++) {
            if (points[i] == null) throw new IllegalArgumentException("Null Element in Array");
            else {
                copied[i] = points[i];
            }
        }

        Arrays.sort(copied);
        Point[] segmentPoints;

        lines = new LineSegment[0];
        int lineIndex = 0;
        for (int i = 0; i < copied.length - 1; i++) {
            if (copied[i] == null) throw new IllegalArgumentException("null Element in points array");
            if (copied[i].compareTo(copied[ i + 1]) == 0) throw new IllegalArgumentException("same Point");
            for (int j = i + 1; j < copied.length - 2; j++) {
                if (copied[j] == null) throw new IllegalArgumentException("null Element in points array");
//                if (copied[i].compareTo(copied[j]) == 0) throw new IllegalArgumentException("same Point");
                double slope12 = copied[i].slopeTo(copied[j]);

                for (int k = j + 1; k < copied.length - 1; k++) {
                    if (copied[k] == null) throw new IllegalArgumentException("null Element in points array");
                    if (copied[j].compareTo(copied[k]) == 0) throw new IllegalArgumentException("same Point");
                    double slope13 = copied[i].slopeTo(copied[k]);
                    if (slope12 != slope13) continue;

                    for (int l = k + 1; l < copied.length; l++) {
                        if (copied[l] == null) throw new IllegalArgumentException("null Element in points array");
                        if (copied[k].compareTo(copied[l]) == 0) throw new IllegalArgumentException("same Point");
                        double slope14 = copied[i].slopeTo(copied[l]);
                        if (slope12 == slope14) {
                            if (lines.length == numberOfSegments) resize(lines.length + 1);
                            segmentPoints = sortPoints(copied[i],copied[j],copied[k],copied[l]);
                            lines[lineIndex] = new LineSegment(segmentPoints[0], segmentPoints[3]);
                            lineIndex ++;
                            numberOfSegments ++;
                        }
                    }
                }
            }
        }
    }


    public int numberOfSegments() {return lines.length;}        // the number of line segments
    // the line segments
    public LineSegment[] segments() {
        return lines.clone();
    }

    public static void main(String[] args) {

//            // read the n points from a file
//            In in = new In(args[0]);
//            int n = in.readInt();
//            Point[] points = new Point[n];
//            for (int i = 0; i < n; i++) {
//                int x = in.readInt();
//                int y = in.readInt();
//                points[i] = new Point(x, y);
//            }
//
//            // draw the points
//            StdDraw.enableDoubleBuffering();
//            StdDraw.setXscale(0, 32768);
//            StdDraw.setYscale(0, 32768);
//            for (Point p : points) {
//                p.draw();
//            }
//            StdDraw.show();
//
//            // print and draw the line segments
//            BruteCollinearPoints collinear = new BruteCollinearPoints(points);
//            for (LineSegment segment : collinear.segments()) {
//                StdOut.println(segment);
//                segment.draw();
//            }
//            StdDraw.show();

//        Point p1 = new Point(3, 1);
//        Point p2 = new Point(4, 2);
//        Point p3 = new Point(5, 3);
//        Point p4 = new Point(6, 4);
//
//        StdDraw.enableDoubleBuffering();
//        StdDraw.setXscale(0, 10);
//        StdDraw.setYscale(0, 10);
//        Point[] p = new Point[4];
//        p[0] = p1;
//        p[1] = p2;
//        p[2] = p3;
//        p[3] = p4;
//        for (Point x : p) {
//            x.draw();
//        }
//
//                Point[] p = new Point[12];
////
//        Point p1 = new Point(7, 1);
//        Point p2 = new Point(8, 1);
//        Point p3 = new Point(1, 1);
//        Point p4 = new Point(1, 5);
//        Point p5 = new Point(3, 5);
//        Point p6 = new Point(2, 6);
//        Point p7 = new Point(9, 1);
//        Point p8 = new Point(11, 1);
//        Point p9 = new Point(14, 1);
//        Point p10 = new Point(1, 7);
//        Point p11 = new Point(0, 0);
//        Point p12 = new Point(8, 1);
//
//        p[0] = p1;
//        p[1] = p2;
//        p[2] = p3;
//        p[3] = p4;
//        p[4] = p5;
//        p[5] = p6;
//        p[6] = p7;
//        p[7] = p8;
//        p[8] = p9;
//        p[9] = p10;
//        p[10] = p11;
//        p[11] = p12;

        Point[] p = new Point[5];

//        Point p1 = new Point(31930, 20728);
//        Point p2 = new Point(31930, 20728);
//        Point p3 = new Point(8308, 124);
//        Point p4 = new Point(7844, 23214);
//        Point p5 = new Point(1602, 19090);

        Point p1 = new Point(23790, 31417);
        Point p2 = new Point(19303, 15997);
        Point p3 = new Point(17248, 25877);
        Point p4 = new Point(2414, 26280);
        Point p5 = new Point(23790, 31417);

        p[0] = p1;
        p[1] = p2;
        p[2] = p3;
        p[3] = p4;
        p[4] = p5;

        BruteCollinearPoints bf = new BruteCollinearPoints(p);
//        for (LineSegment lS: bf.segments()) {
//            lS.draw();
//        }
//
//        StdDraw.show();

    }

}
