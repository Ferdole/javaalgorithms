import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

public class FastCollinearPoints {

    private LineSegment[] lines;

    private void resize(int capacity) {
        LineSegment[] aux = new LineSegment[capacity];
        for (int i = 0; i < lines.length; i++) {
            aux[i] = lines[i];
        }
        lines = aux;
    }

    private void checkNullElements(Point[] points) {
        for (Point p:points) {
            if (p == null) throw new IllegalArgumentException("Null point");
        }
    }

    private void checkDuplicateElements(Point[] points) {
        for (int i = 0; i < points.length - 1; i++) {
            if  (points[i].compareTo(points[i + 1]) == 0) throw new IllegalArgumentException("Duplicate Points");
        }
    }

    public FastCollinearPoints(Point[] points) {

        if (points == null) throw new IllegalArgumentException("Null points");
        else checkNullElements(points);

        // two point are collinear when they have the same slope
        this.lines = new LineSegment[0];
        int lineSegmentIndex = 0;
        Point[] sortedPoints = points.clone();
        Arrays.sort(sortedPoints);
        checkDuplicateElements(sortedPoints);

        double currentSlope, previousSlope;
        LinkedList<Point> segment;
        List<LineSegment> segments = new LinkedList<>();

        for (int i = 0; i < sortedPoints.length - 2 ; i++) {
            Point[] sortedBySlope = sortedPoints.clone();
            Arrays.sort(sortedBySlope, sortedBySlope[i].slopeOrder());
            /*
            /   We sort by the slope that the current point does with all the other points. The order of the points after\
            the slope order is kept. IE the order of the points is kept except the first one in the array will always be \
            the point from which the lines origin.
             */
            Point currentPoint = sortedBySlope[0];

            previousSlope = currentSlope = currentPoint.slopeTo(sortedBySlope[1]) ;
            boolean skipSlope = false;
            if (currentPoint.compareTo(sortedBySlope[1]) > 0) {
                skipSlope = true;
            }

            segment = new LinkedList<>();

            for (int j = 1; j < sortedBySlope.length; j++) {
                currentSlope = currentPoint.slopeTo(sortedBySlope[j]);
                if (currentSlope == previousSlope) {
                    if ( skipSlope ) {
//                        System.out.println(String.format("Point %s already in a segment with point %s", currentPoint, sortedBySlope[j]));
//                        previousSlope = currentSlope;
                        continue;

                    }
                } else {

                    if (segment.size() >= 3){
//                        if (lines.length == lineSegmentIndex) resize(lineSegmentIndex + 1);
//                        lines[lineSegmentIndex++] = new LineSegment(currentPoint, segment.removeLast());
                        segments.add(new LineSegment(currentPoint, segment.removeLast()));
                        segment = new LinkedList<>();
                    }

                    previousSlope = currentSlope;
                    if (currentPoint.compareTo(sortedBySlope[j]) > 0) {
                        skipSlope = true;
                        continue;
                    }

                    skipSlope = false;
                    segment = new LinkedList<>();
                    // if that point is not the first in the slope order then that segment was added

//                    if (currentPoint.compareTo(sortedBySlope[j]) > 0 && acc == 0){
////                        System.out.println(String.format("Point %s already in a segment with point %s", currentPoint, sortedBySlope[j]));
//                        skipSlope = true;
//                        segment = new LinkedList<>();
//                        segment.addFirst(currentPoint);
//                        acc = 0;
//                        continue;
//                    }

                }
                segment.addLast(sortedBySlope[j]);

            }

            if (segment.size() >= 3){
//                if (lines.length == lineSegmentIndex) resize(lineSegmentIndex + 1);
//                lines[lineSegmentIndex++] = new LineSegment(currentPoint, segment.removeLast());
                segments.add(new LineSegment(currentPoint, segment.removeLast()));
            }
        }
        lines = segments.toArray(new LineSegment[0]);

    }

    // finds all line segments containing 4 or more points
    public           int numberOfSegments() {return lines.length;}        // the number of line segments
    public LineSegment[] segments(){return lines.clone();}                // the line segments

    public static void main(String[] args) {

//         read the n points from a file
        In in = new In(args[0]);
        int n = in.readInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }

        // draw the points
        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
        }
        StdDraw.show();

        // print and draw the line segments
        FastCollinearPoints collinear = new FastCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }
        StdDraw.show();

//////        LinkedList<Point> linkedInt = new LinkedList<>();
//////                linkedInt.addFirst(p1);
//////        linkedInt.addFirst(p2);
//////        linkedInt.addFirst(p3);
//////        System.out.println(linkedInt.contains(p3));
//////        System.out.println(linkedInt.size());
//////        System.out.println(0.0 == -0.0);
//        Point[] p = new Point[12];
////
//        Point p1 = new Point(7, 1);
//        Point p2 = new Point(8, 1);
//        Point p3 = new Point(1, 1);
//        Point p4 = new Point(1, 5);
//        Point p5 = new Point(3, 5);
//        Point p6 = new Point(2, 6);
//        Point p7 = new Point(9, 1);
//        Point p8 = new Point(11, 1);
//        Point p9 = new Point(14, 1);
//        Point p10 = new Point(1, 7);
//        Point p11 = new Point(0, 0);
//        Point p12 = new Point(18, 1);
//
//        p[0] = p1;
//        p[1] = p2;
//        p[2] = p3;
//        p[3] = p4;
//        p[4] = p5;
//        p[5] = p6;
//        p[6] = p7;
//        p[7] = p8;
//        p[8] = p9;
//        p[9] = p10;
//        p[10] = p11;
//        p[11] = p12;
//////        Point p1 = new Point(10000,  0);
//////        Point p2 = new Point(0,  10000);
//////        Point p3 = new Point(3000,   7000);
//////        Point p4 = new Point(7000,   3000);
//////        Point p5 = new Point(20000,  21000);
//////        Point p6 = new Point(3000,   4000);
//////        Point p7 = new Point(14000,  15000);
//////        Point p8 = new Point(6000,  7000);
//////        Point p1 = new Point(8502,  17130);
//////        Point p2 = new Point(13756,  17130);
//////        Point p3 = new Point(13553,   17130);
//////        Point p4 = new Point(1947,   17130);
//////
////        Point p1 = new Point(1000,  1000);
////        Point p2 = new Point(2000,  2000);
////        Point p3 = new Point(3000,   3000);
////        Point p4 = new Point(4000,   4000);
////        Point p5 = new Point(5000, 5000);
////        Point p6 = new Point(6000, 6000);
////        Point p7 = new Point(7000, 7000);
////        Point p8 = new Point(8000, 8000);
////        Point p9 = new Point(9000, 9000);
////
////        p[0] = p1;
////        p[1] = p2;
////        p[2] = p3;
////        p[3] = p4;
////        p[4] = p5;
////        p[5] = p6;
////        p[6] = p7;
////        p[7] = p8;
////        p[8] = p9;
////
////        Point p1 = new Point(7, 2);
////        Point p2 = new Point(9, 0);
////        Point p3 = new Point(2, 2);
////        Point p4 = new Point(8, 0);
////        Point p5 = new Point(9, 2);
////        Point p6 = new Point(6, 7);
////        Point p7 = new Point(3, 5);
////        Point p8 = new Point(6, 2);
////        Point p9 = new Point(7, 0);
////        Point p10 = new Point(5, 2);
////
////        p[0] = p1;
////        p[1] = p2;
////        p[2] = p3;
////        p[3] = p4;
////        p[4] = p5;
////        p[5] = p6;
////        p[6] = p7;
////        p[7] = p8;
////        p[8] = p9;
////        p[9] = p10;
////
//        FastCollinearPoints fp = new FastCollinearPoints(p);
//        for (LineSegment x: fp.segments()) {
//            System.out.println(x);
//        }
    }

}
