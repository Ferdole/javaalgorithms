import java.util.Arrays;
import java.util.Comparator;
import edu.princeton.cs.algs4.StdDraw;

public class Point implements Comparable<Point> {

    private final int x;     // x-coordinate of this point
    private final int y;     // y-coordinate of this point

    /**
     * Initializes a new point.
     *
     * @param  x the <em>x</em>-coordinate of the point
     * @param  y the <em>y</em>-coordinate of the point
     */
    public Point(int x, int y) {
        /* DO NOT MODIFY */
        this.x = x;
        this.y = y;
    }

    /**
     * Draws this point to standard draw.
     */
    public void draw() {
        /* DO NOT MODIFY */
        StdDraw.point(x, y);
    }

    /**
     * Draws the line segment between this point and the specified point
     * to standard draw.
     *
     * @param that the other point
     */
    public void drawTo(Point that) {
        /* DO NOT MODIFY */
        StdDraw.line(this.x, this.y, that.x, that.y);
    }

    private boolean PointBelongsToSegment(Point A, Point B) {
        double nom1 = this.y - A.y;
        double denom1 = B.y - A.y;
        double nom2 = this.x - A.x;
        double denom2 = B.x - A.x;
        double slope = denom1 / denom2;
        return nom1 == slope * nom2;
    }
    /**
     * Returns the slope between this point and the specified point.
     * Formally, if the two points are (x0, y0) and (x1, y1), then the slope
     * is (y1 - y0) / (x1 - x0). For completeness, the slope is defined to be
     * +0.0 if the line segment connecting the two points is horizontal;
     * Double.POSITIVE_INFINITY if the line segment is vertical;
     * and Double.NEGATIVE_INFINITY if (x0, y0) and (x1, y1) are equal.
     *
     * @param  that the other point
     * @return the slope between this point and the specified point
     */
    public double slopeTo(Point that) {
        double nominator = (that.y - this.y);
        double denominator = (that.x - this.x);

        if (denominator == 0.0 && nominator != 0) return Double.POSITIVE_INFINITY;  // vertical line segment
        else if (nominator == 0.0 && denominator == 0.0) return Double.NEGATIVE_INFINITY;  // equal Points
        else if (nominator == 0.0) return 0.0;
        else return nominator/denominator;
        /* YOUR CODE HERE */
    }

    /**
     * Compares two points by y-coordinate, breaking ties by x-coordinate.
     * Formally, the invoking point (x0, y0) is less than the argument point
     * (x1, y1) if and only if either y0 < y1 or if y0 = y1 and x0 < x1.
     *
     * @param  that the other point
     * @return the value <tt>0</tt> if this point is equal to the argument
     *         point (x0 = x1 and y0 = y1);
     *         a negative integer if this point is less than the argument
     *         point; and a positive integer if this point is greater than the
     *         argument point
     */
    public int compareTo(Point that) {
        if (this.y - that.y == 0) {
//             return Integer.compare(this.x - that.x, 0); // Integer comparison
            return this.x - that.x;
        } else return this.y - that.y;
        /* YOUR CODE HERE */
    }

    /**
     * Compares two points by the slope they make with this point.
     * The slope is defined as in the slopeTo() method.
     *
     * @return the Comparator that defines this ordering on points
     */
    public Comparator<Point> slopeOrder() {
        Point a = this;
        Comparator<Point> p = new Comparator<Point>()
        {
            @Override
            public int compare(Point o1, Point o2) {
                double o1ThisSlope = a.slopeTo(o1);
                double o2ThisSlope = a.slopeTo(o2);
                if (o1ThisSlope == o2ThisSlope) return 0;
                else if (o1ThisSlope - o2ThisSlope > 0) return 1;
                else return -1;
            }
        };
        return p;
    }


    /**
     * Returns a string representation of this point.
     * This method is provide for debugging;
     * your program should not rely on the format of the string representation.
     *
     * @return a string representation of this point
     */
    public String toString() {
        /* DO NOT MODIFY */
        return "(" + x + ", " + y + ")";
    }

    /**
     * Unit tests the Point data type.
     */
    public static void main(String[] args) {

//        System.out.println(Double.NEGATIVE_INFINITY - Double.POSITIVE_INFINITY);
        Point p1 = new Point(7, 1);
        Point p2 = new Point(3, 5);
        Point p3 = new Point(8, 1);
        Point p4 = new Point(1, 1);
        Point p5 = new Point(1, 5);
        Point p6 = new Point(1, 5);
        Point start;
        System.out.println(p1.slopeOrder());

        Comparator<Point> cp = p1.slopeOrder();

        System.out.println(cp.compare(p2, p3));

        System.out.println(p5.compareTo(p6));
        System.out.println(p5.equals(p6));

        System.out.println(p1.compareTo(p2));
        System.out.println(String.format("Slope %s to %s: %f" ,p1, p2,p1.slopeTo(p2)));
        System.out.println(String.format("Slope %s to %s: %f" ,p1, p3,p1.slopeTo(p3)));
        System.out.println(String.format("Slope %s to %s: %f" ,p3, p4,p3.slopeTo(p4)));
        // draw the points
        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 7);
        StdDraw.setYscale(0, 7);

        LineSegment z = new LineSegment(p1, p2);

        Point[] p = new Point[5];
        p[0] = p1;
        p[1] = p2;
        p[2] = p3;
        p[3] = p4;
        p[4] = p5;

        start = p[0];
        start = p[3];
        System.out.println(start + " and " + p[0]);
        Arrays.sort(p);
//        Arrays.sort(p);
        for (Point x: p) {
            System.out.println(x);;
        }
//        p1.draw();
//
//        StdDraw.show();
        /* YOUR CODE HERE */

    }
}