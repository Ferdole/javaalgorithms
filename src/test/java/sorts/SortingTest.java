package sorts;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Assert;


import java.util.Arrays;

public class SortingTest{
    protected static int value1, value2;

    @BeforeClass
    public static void oneTimeSetUp() {
        value1 = 2;
        value2 = 4;
        // one-time initialization code
    }

    @Test
    public void testMergeSort() {
        String[] toBeSorted = new String[]{"b", "h", "e", "z", "m", "h", "p"};
        String[] expected = new String[]{"b", "e", "h", "h", "m", "p", "z"};

        MergeSort.sort(toBeSorted);

        System.out.println(String.join(",", toBeSorted));
        Assert.assertEquals(expected, toBeSorted);
    }

    @Test
    public void testAdd() {
        double result = value1 + value2;
        Assert.assertEquals(6, result, 0.0);
    }
}
