package quickfind;

public class QuickFindUF {

    private int [] id;

    public QuickFindUF(int N)
    {
        id = new int[N];
        for (int i = 0; i < N; i++)
            id[i] = i;
    }

    /* down from here quick-union, tree like implementation
    * - Trees can get tall.
    * - Find too expensive (could be N array accesses) */
    // for tree like implementation of quick find
    private int root(int i){
        while (i != id[i]) i = id[i];
        return i;
    }

    public boolean rootConnected(int p, int q){
        return root(p) == root(q);
    }

    public void rootUnion(int p, int q){
        int i = root(p);
        int j = root(q);
        id[i] = j;
    }

    /* down from here array implementation of quick find
    * - union too expensive (N array accesses)
    * - Trees flat, but too expensive to keep them flat */
    // array implementation of quick find
    public boolean connected(int p, int q){
        return id[p] == id[q];
    }

    // array type of implementation of quick find. poor performance N^2
    public void union(int p, int q){
        int pid = id[p];
        int qid = id[q];
        for (int i = 0; i < id.length; i++)
            if (id[i] == id[p]) id[i] = qid;
    }
}
