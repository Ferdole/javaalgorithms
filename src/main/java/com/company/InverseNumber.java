package com.company;

public class InverseNumber {

    public static StringBuilder reverseDecimalInteger(int a) {
        int rank = 10;
        int c = a;
        StringBuilder newNumber = new StringBuilder();
        int digit;

        while (c > 0){
            digit = c % rank;
            newNumber.append(digit);
            c /= rank;

        }

        return newNumber;
    }

    public static void main(String[] args) {
        System.out.println("hello" + "3");
        int d = 2344;
        d %= 10;
        System.out.println(d);

        System.out.println(reverseDecimalInteger(14570));

    }

}
