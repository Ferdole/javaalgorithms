package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StringTesting {

    public static void main(String[] args) {
        String s = "B" + "n" + "c";
        char z = s.charAt(0);
        int b = z;
        System.out.println(b);

        List<String> az = new ArrayList<>(Collections.singletonList("dorel"));
        az.add("mut");
        System.out.println(String.join(", ", az));
    }
}
