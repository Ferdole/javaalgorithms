package com.company;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;

public class Main {

    public static void main(String[] args) {


        try {
            // create object mapper instance
            ObjectMapper mapper = new ObjectMapper();

            // convert JSON file to map
            Map<?, ?> map = mapper.readValue(Paths.get("/mnt/app/javaalgorithms/src/main/resources/book.json").toFile(), Map.class);

            // print map entries
            for (Map.Entry<?, ?> entry : map.entrySet()) {
                System.out.println(entry.getKey() + "=" + entry.getValue());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");

        Date date = new Date(System.currentTimeMillis());

        Calendar cal = Calendar.getInstance();
        cal.set(1994, Calendar.JANUARY, 31, 17, 25);

        System.out.println(formatter.format(cal.getTime()));

        System.out.println("HelloWorldw");
        int[] ia = new int[20];
        int[] id_array = new int[30];

        for (int i = 0; i < ia.length; i++) {
            for (int j = 0; j < id_array.length; j++) {
                if (j == 4) break;
            }
//            System.out.println(String.format("i %d values", i));
        }

    }
}
