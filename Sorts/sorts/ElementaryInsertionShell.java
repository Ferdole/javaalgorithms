package sorts;

import edu.princeton.cs.algs4.StdRandom;

public class ElementaryInsertionShell {

    private static boolean less(int x, int y){
        return x < y;
    }

    /**
     * Exchange element at index i with j;
     *
     * @param a
     * @param i
     * @param j
     */
    private static void exch(int[] a, int i, int j){
        int aux;
        aux = a[j];
        a[j] = a[i];
        a[i] = aux;
    }

    public int[] insertionSortArray(int [] a) {
        int aux = 0;
        for (int i = 1; i < a.length; i++) {


            System.out.println(a[i]);
            for (int j = i; j >= 1 && less(a[j], a[j - 1]); j--) {

                exch(a, j, j - 1);

            }
        }

        return a;
    }

    public int[] shellsort(int []a){

        int N = a.length;

        int h = 1;
        // generate greatest step given array length
        while( h < N/3) h = h * 3 + 1;

        while (h >= 1) {

            for (int i = h; i < N; i++) {
                for (int j = i; j >= h && less(a[j], a[j - 1]); j = -h)
                    exch(a, j, j - h);
            }
            h = h / 3;
        }
        return a;
    }

    public static void main(String[] args) {
        int[] a = new int[20];
        for (int i = 0; i < a.length; i++) {
            a[i]= StdRandom.uniform(0, 30);
        }
        ElementaryInsertionShell test = new ElementaryInsertionShell();
        a = test.insertionSortArray(a);

        System.out.println("Sorted:");
        for (int b: a) {
            System.out.println(b);
        }
    }
}
