package sorts;

import edu.princeton.cs.algs4.StdRandom;

public class MergeSort {

//    public MergeSort(){
//        System.out.println("Building class...");
//    }

    private static void merge(Comparable[] a, Comparable[] aux, int lo, int mid, int hi){

        for (int i = 0; i < a.length; i++) {
            aux[i] = a[i];
        }

        int i = lo, j = mid + 1;
        // hi is length of previous array
        for (int k = lo; k <= hi; k++) {

            if (j > hi)                a[k] = aux[i++];
            else if (i > mid)          a[k] = aux[j++];
            else if (aux[j].compareTo(aux[i]) < 0)  a[k] = aux[j++];
            else                       a[k] = aux[i++];

        }
    }

    private static void sort(Comparable[] a, Comparable[] aux, int lo, int hi){

        if (hi <= lo) return;
        int mid = lo + (hi - lo) / 2;
        sort(a, aux, lo, mid);
        sort(a, aux, mid + 1, hi);
        merge(a, aux, lo, mid, hi);
    }

    private static void sortCutoff(Comparable[] a, Comparable[] aux, int lo, int hi){


//        if (hi <= lo + CUTOFF - 1)
//        {
//            Insertion.sort(a, lo, hi);
//            return;
//        }

        if (hi <= lo) return;
        int mid = lo + (hi - lo) / 2;
        sort(a, aux, lo, mid);
        sort(a, aux, mid + 1, hi);
        merge(a, aux, lo, mid, hi);
    }

    public static void sort(Comparable[] a){
        Comparable[] aux = new Comparable[a.length];
        sort(a, aux, 0, a.length - 1);
    }

    public static void main(String[] args) {
        Comparable<Integer>[] a = new Comparable[20];
        for (int i = 0; i < a.length; i++) {
            a[i]= StdRandom.uniform(0, 30);
        }
//        MergeSort test = new MergeSort();
        MergeSort.sort(a);

        System.out.println("Sorted:");
        for (Comparable<Integer> b: a) {
            System.out.println(b);
        }
    }

}
