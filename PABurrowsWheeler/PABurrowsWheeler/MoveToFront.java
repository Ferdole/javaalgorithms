package PABurrowsWheeler;

import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;


public class MoveToFront {

    private static final int ALPHABET_SIZE = 256;
    private char[] moveToFrontArray;
    private int[] whereCharActuallyIs;

    public MoveToFront(){
        moveToFrontArray = new char[ALPHABET_SIZE];
        whereCharActuallyIs = new int[ALPHABET_SIZE];

        for (int i=0; i < ALPHABET_SIZE; i ++) {
            moveToFrontArray[i] = (char) i;
            whereCharActuallyIs[i] = i;
        }
    }

    private void moveToFrontEncode(int c) {

        int charIndex = whereCharActuallyIs[c];
//        System.out.println("Char: " + c);
        BinaryStdOut.write((byte) charIndex);
//        System.out.println(charIndex);
        char charToMove = moveToFrontArray[charIndex];

        if (charIndex==0) return;

        char previous = moveToFrontArray[0];
        char next;

        for (int i=0; i < charIndex; i++) {
            next = moveToFrontArray[i+1];
            moveToFrontArray[i+1] = previous;
            whereCharActuallyIs[previous] ++;
            previous = next;
        }

        whereCharActuallyIs[c] = 0;
        moveToFrontArray[0] = charToMove;
    }

    private void moveToFrontDecode(int c) {

        char decoded = moveToFrontArray[c];
        int charIndex = whereCharActuallyIs[decoded];
        BinaryStdOut.write((byte) decoded);
//        System.out.println(decoded);

        if (charIndex==0) return;

        char previous = moveToFrontArray[0];
        char next;

        for (int i=0; i < charIndex; i++) {
            next = moveToFrontArray[i+1];
            moveToFrontArray[i+1] = previous;
            whereCharActuallyIs[previous] ++;
            previous = next;
        }

        whereCharActuallyIs[decoded] = 0;
        moveToFrontArray[0] = decoded;
    }

    public static void encode(){
        MoveToFront e = new MoveToFront();

        while (!BinaryStdIn.isEmpty()){
            e.moveToFrontEncode(BinaryStdIn.readChar());
        }
        BinaryStdOut.close();
    }

    public static void decode(){
        MoveToFront e = new MoveToFront();

        while (!BinaryStdIn.isEmpty()){
            e.moveToFrontDecode(BinaryStdIn.readChar());
        }
        BinaryStdOut.close();
    }

    // if args[0] is '-', apply move-to-front encoding
    // if args[0] is '+', apply move-to-front decoding
    public static void main(String[] args) {
        if (args[0].equals("-")) {
            encode();
        } else if (args[0].equals("+")) {
            decode();
        } else {
            System.out.println("Usage: '-' encoding, '+' decoding");
        }

//        PABurrowsWheeler.MoveToFront e = new PABurrowsWheeler.MoveToFront();
//
//        e.moveToFrontEncode('A');
//        e.moveToFrontEncode('B');
//        e.moveToFrontEncode('R');
//        e.moveToFrontEncode('A');
//        e.moveToFrontEncode('C');
//        e.moveToFrontEncode('A');
//        e.moveToFrontEncode('D');
//        e.moveToFrontEncode('A');
//        e.moveToFrontEncode('B');
//        e.moveToFrontEncode('R');
//        e.moveToFrontEncode('A');
//        e.moveToFrontEncode('!');
//
//        e.moveToFrontEncode('Z');
//        e.moveToFrontEncode('E');
//        e.moveToFrontEncode('B');
//        e.moveToFrontEncode('R');
//        e.moveToFrontEncode('A');
//
//        PABurrowsWheeler.MoveToFront d = new PABurrowsWheeler.MoveToFront();
//
//        d.moveToFrontDecode(65);
//        d.moveToFrontDecode(66);
//        d.moveToFrontDecode(82);
//        d.moveToFrontDecode(2);
//        d.moveToFrontDecode(68);
//        d.moveToFrontDecode(1);
//        d.moveToFrontDecode(69);
//        d.moveToFrontDecode(1);
//        d.moveToFrontDecode(4);
//        d.moveToFrontDecode(4);
//        d.moveToFrontDecode(2);
//        d.moveToFrontDecode(38);

//        d.moveToFrontDecode(90);
//        d.moveToFrontDecode(70);
//        d.moveToFrontDecode(68);
//        d.moveToFrontDecode(83);
//        d.moveToFrontDecode(69);

    }
}