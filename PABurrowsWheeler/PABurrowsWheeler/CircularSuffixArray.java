package PABurrowsWheeler;

import java.util.HashMap;
import java.util.List;
import java.util.Arrays;
import java.util.stream.Collectors;
import edu.princeton.cs.algs4.QuickBentleyMcIlroy;

public class CircularSuffixArray {


    // TODO: this is not an efficient implementation
    private final int[] originalToSortedIndexes;
    private final int sLength;
    private String t;

    // circular suffix array of s
    public CircularSuffixArray(String s){

        if (s == null) throw new IllegalArgumentException("Constructor Argument connot be null");

        sLength = s.length();
        originalToSortedIndexes = new int[s.length()];

        String[] originalSuffixes = createSuffixArray(s);

        int suffixSortedIndex;
        StringBuilder t = new StringBuilder();
        List<String> sortedOriginal = Arrays.stream(originalSuffixes).sorted().collect(Collectors.toList());
        HashMap<String, Integer> originalSuffixIndexMap = new HashMap<>();

        for (String ss : sortedOriginal) {
            t.append(ss.charAt(sLength-1));
        }

        int j;
        String currentOriginalSuffix;
        for (int i=0; i < s.length(); i ++) {

            currentOriginalSuffix = originalSuffixes[i];

            suffixSortedIndex = sortedOriginal.indexOf(currentOriginalSuffix);
            if (originalSuffixIndexMap.containsKey(currentOriginalSuffix)) {
                j = originalSuffixIndexMap.get(currentOriginalSuffix) + 1;
                originalSuffixIndexMap.put(currentOriginalSuffix, j);
            } else {
                j = suffixSortedIndex;
                originalSuffixIndexMap.put(currentOriginalSuffix, suffixSortedIndex);
            }

            originalToSortedIndexes[j] = i;
        }

//        originalToSortedIndexess = createOriginalToSortedIndexes();
    }

    private String[] createSuffixArray(String s) {

        StringBuilder sBuffer;
        char[] sArray = s.toCharArray();
        String[] originalSuffixes = new String[s.length()];

        int i, j, m;
        for (i=0; i < s.length(); i ++) {
            sBuffer = new StringBuilder();

            for (j=i; j < s.length(); j ++) {
                sBuffer.append(sArray[j]);
            }

            for (m=0; m < i; m ++) {
                sBuffer.append(sArray[m]);
            }

            String sBufferStr = sBuffer.toString();

//            System.out.println(sBufferStr);
            originalSuffixes[i] = sBufferStr;
        }

        return originalSuffixes;
    }

    // length of s
    public int length(){
        return sLength;
    }

    // returns index of ith sorted suffix
    public int index(int i){

        if (i < 0) throw new IllegalArgumentException("i < 0");
        else if ( i >= sLength) throw new IllegalArgumentException("i >= string length");
        else return originalToSortedIndexes[i];
    }

    // unit testing (required)
    public static void main(String[] args) {

        String st = "dorel";

        CircularSuffixArray s = new CircularSuffixArray("ABRACADABRA!");

        //periodic string
//        PABurrowsWheeler.CircularSuffixArray s = new PABurrowsWheeler.CircularSuffixArray("cuscus");
//        PABurrowsWheeler.CircularSuffixArray s = new PABurrowsWheeler.CircularSuffixArray("BBAA");

        System.out.println(s.index(0));
//        String[] a = s.createSuffixArray("ABRACADABRA!");
//
//        List<String> sorted = Arrays.stream(a).sorted().collect(Collectors.toList());

//        System.out.println(sorted);
//        System.out.println(s.length());
    }

}