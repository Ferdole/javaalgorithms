package PABurrowsWheeler;

import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;
import edu.princeton.cs.algs4.QuickBentleyMcIlroy;

import java.util.Arrays;

public class BurrowsWheeler {

    private static final int ALPHABET_SIZE = 256;

    // apply Burrows-Wheeler transform,
    // reading from standard input and writing to standard output
    public static void transform() {
        final String s = BinaryStdIn.readString();
        final int length = s.length();
        final int offset = s.length() - 1;
        final CircularSuffixArray a = new CircularSuffixArray(s);
        for (int i = 0; i < length; i++) {
            if (a.index(i) == 0) {
                BinaryStdOut.write(i);
                break;
            }
        }
        for (int i = 0; i < a.length(); i++)
            BinaryStdOut.write(s.charAt((a.index(i) + offset) % length));
        BinaryStdOut.close();
    }

    // apply Burrows-Wheeler inverse transform,
    // reading from standard input and writing to standard output
    public static void inverseTransform() {
        final int first = BinaryStdIn.readInt();
        final char[] str = BinaryStdIn.readString().toCharArray();
        final int[] counts = new int [ALPHABET_SIZE];
        final int[][] endsLists = new int [ALPHABET_SIZE][];
        for (final char c : str) ++counts[c];
        for (int i = 0; i < endsLists.length; i++)
            endsLists[i] = new int [counts[i]];
        Arrays.fill(counts, 0);
        for (int i = 0; i < str.length; i++) {
            final char c = str[i];
            endsLists[c][counts[c]++] = i;
        }
        int pos = 0;
        for (char c = 0; c < ALPHABET_SIZE; c++)
            for (int i = 0; i < counts[c]; i++)
                str[pos++] = c;
        final int[] next = new int [str.length];
        Arrays.fill(counts, 0);
        for (int i = 0; i < str.length; i++) {
            final char c = str[i];
            next[i] = endsLists[c][counts[c]++];
        }
        for (int i = first, k = 0; k < str.length; i = next[i], k++)
            BinaryStdOut.write(str[i]);
        BinaryStdOut.close();
    }

    // if args[0] is "-", apply Burrows-Wheeler transform
    // if args[0] is "+", apply Burrows-Wheeler inverse transform
    public static void main(String[] args){
        if (args[0].equals("-")) {
            transform();
        } else if (args[0].equals("+")) {
            inverseTransform();
        } else {
            System.out.println("Usage: '-' encoding, '+' decoding");
        }

//        String s = "asdfgh";
//
//        int length = s.length();
//        int offset = s.length() - 1;
//        int n;
//        for (int i=0; i < length; i ++) {
//            n = (i + offset) % length;
//            System.out.println(n);
//            System.out.println(s.charAt((n)));
//        }
    }

}