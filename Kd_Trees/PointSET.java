import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.RectHV;

import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

public class PointSET {

    private TreeSet<Point2D> nodeSet;

    private void checkNull(Object o) {
        if (o == null) throw new IllegalArgumentException("Null argument");
    }

        // construct an empty set of points
    public PointSET()
    {
        nodeSet = new TreeSet<>();
    }

    // is the set empty
    public boolean isEmpty(){
        return nodeSet.isEmpty();
    }

    // number of points in the set
    public int size(){
        return nodeSet.size();
    }

    // add the point to the set (if it is not already in the set
    public void insert(Point2D p){
        checkNull(p);
        nodeSet.add(p);
    }

    // does the set contain point p?
    public boolean contains(Point2D p)
    {
        checkNull(p);
        return nodeSet.contains(p);
    }

    // draw all points to standard draw
    public void draw() {
        for (Point2D p : nodeSet) {

        }
    }

    // all points that are inside the rectangle (or on the boundary)
    public Iterable<Point2D> range(RectHV rect) {

        checkNull(rect);
        List<Point2D> contained = new LinkedList<>();

        for (Point2D p: nodeSet) {
            if (rect.contains(p)) contained.add(p);
        }

        return contained;
    }

    public Point2D nearest(Point2D p)
    {
        checkNull(p);
        if (isEmpty()) return null;
        double currentDistance;
        double candidateDistance = p.distanceTo(nodeSet.first());
        Point2D candidate = nodeSet.first();


        for (Point2D cp: nodeSet) {
            currentDistance = p.distanceTo(cp);
            if (currentDistance < candidateDistance) {
                candidateDistance = currentDistance;
                candidate = cp;
            }
        }

        return candidate;
    }

    public static void main(String[] args) {

        StdDraw.setPenRadius(0.005);
        StdDraw.point(0.5, 0.5);
        StdDraw.point(0.7, 0.1);
        StdDraw.point(0.1, 0.2);
        StdDraw.setPenColor(StdDraw.MAGENTA);
        StdDraw.line(0.2, 0.2, 0.8, 0.2);

        StdDraw.setPenRadius(0.01);
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.point(0.6, 0.2);
        PointSET points = new PointSET();
        points.insert(new Point2D(4, 5));
        points.insert(new Point2D(1, 2));
        points.insert(new Point2D(1, 7));
        points.insert(new Point2D(2, 3));
        points.insert(new Point2D(8, 7));

        System.out.println(points.nearest(new Point2D(8, 7)));


    }

}
