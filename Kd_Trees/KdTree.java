import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdDraw;

import java.util.LinkedList;
/**
 * Works, consumes not that much memory
 * IT IS UGLY
 * Refactor with the ideas seen here https://github.com/mincong-h/algorithm-princeton/blob/master/kdtree/KdTree.java
 *
 * */
public class
KdTree {
    private Node root;

    /**
     * The keys in the tree should alternate through levels.  Start from with the x coordinate and alternate
     * - we should keep track if we are at x or y. Maybe even/uneven levels.
     * - when we divide planes we should keep in mind the range that we can divide and draw the line
     *
     *
     * */
    private class Node {

        private Point2D key;
        private double value;
        private Node left, right;
        private boolean coord;
        private int size;

        private Node(Point2D key, boolean coord, int size){
            this.key = key;
            this.value = value;
            this.coord = coord;
            this.size = size;
        }
    }

    private void checkNull(Object o) {
        if (o == null) throw new IllegalArgumentException("Calling function with null Argument!");
    }

    // construct an empty set of points
    public KdTree()
    {
    }

    // is the set empty
    public boolean isEmpty() {

        return size(root) == 0;
    }

    private int size(Node x) {
        if (x == null) return 0;
        return x.size;
    }

    // number of points in the set
    public int size(){
        return size(root);
    }

    private Node put(Node x, Point2D key, boolean coord){

        // coord will be true when key is the x coord and false when key is y coord


        if (x == null) {
            return new Node(key, coord, 1);
        }

        double cmp;
        if (!coord) {
            cmp = Point2D.Y_ORDER.compare(key, x.key);
        } else cmp = Point2D.X_ORDER.compare(key, x.key);

        if (cmp < 0) x.left = put(x.left, key, !coord);
        else if (cmp > 0) x.right = put(x.right, key, !coord);
        // just update the value -> we don't need to keep points in a set
        else {
            if (!x.coord) {
                cmp = Point2D.X_ORDER.compare(key, x.key);
            } else
                cmp = Point2D.Y_ORDER.compare(key, x.key);
            if (cmp < 0) x.left = put(x.left, key, !coord);
            else if (cmp > 0) x.right = put(x.right, key, !coord);
        }

        x.size = size(x.left) + size(x.right) + 1;

        return x;
    }

    // add the point to the set (if it is not already in the set
    public void insert(Point2D p){

        checkNull(p);
        // add Point to set and KdTree -- maybe add it only to KdTree and check there if it is duplicate
        root = put(root, p, true);

    }

    private Point2D get(Node x, Point2D key) {

        while (x != null) {
            int cmp;
            if (x.coord) {
                cmp = Point2D.X_ORDER.compare(key, x.key);
            } else
                cmp = Point2D.Y_ORDER.compare(key, x.key);

            if (cmp < 0) x = x.left;
            else if (cmp > 0) x = x.right;
            else {
                // previous coordinates are equal so we compare the other
                if (!x.coord) {
                    cmp = Point2D.X_ORDER.compare(key, x.key);
                } else
                    cmp = Point2D.Y_ORDER.compare(key, x.key);
                if (cmp < 0) x = x.left;
                else if (cmp > 0) x = x.right;
                else return x.key;
            }
        }

        return null;
    }

    // does the set contain point p?
    public boolean contains(Point2D p)
    {
        checkNull(p);
        Point2D x = get(root, p);
        return x != null;
    }

    private void drawLine(double x0, double y0, double x1, double y1) {
        StdDraw.setPenRadius(0.005);
        StdDraw.line(x0, y0, x1, y1);
    }

    private void drawPoint(double x, double y){
        StdDraw.setPenRadius(0.05);
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.point(x, y);
    }

    private void draw(Node x, double leftBoundary, double rightBoundary, double upBoundary, double downBoundary) {

        // if coord is true then we draw a vertical line, parallel to oY axis else a horizontal parallel to oX axis
        // boundary is the limit of the line
        if (x == null) return;

        // we draw a vertical line
        if (x.coord) {
            drawPoint(x.key.x(), x.key.y());
            StdDraw.setPenColor(StdDraw.RED);
            drawLine(x.key.x(), downBoundary, x.key.x(), upBoundary);
            draw(x.left, leftBoundary, x.key.x(), upBoundary, downBoundary);
            draw(x.right, x.key.x(), rightBoundary, upBoundary, downBoundary);
        }
        // we draw a horizontal line
        else {
            drawPoint(x.key.x(), x.key.y());
            StdDraw.setPenColor(StdDraw.BLUE);
            drawLine(leftBoundary, x.key.y(), rightBoundary, x.key.y());
            draw(x.left, leftBoundary, rightBoundary, x.key.y(), downBoundary);
            draw(x.right, leftBoundary, rightBoundary, upBoundary, x.key.y());
        }

    }

    // draw all points to standard draw
    public void draw(){
        draw(root, 0, 1, 1, 0);
    }

    private Point2D distanceCheck(Point2D p, Node x, double xmin, double ymin, double xmax, double ymax,
                                 Point2D candidate, double candidateDistance)
    {
        /*
         * The xmin, ymin, xmax, ymax coordinates are for the whole rectangle where the point is situated
         * When we calculate the distance we need to limit it to the half that we are interested in
         * */
        if (x == null) return candidate;
        double xCoord = x.key.x();
        double yCoord = x.key.y();
        double distance;
        distance = p.distanceTo(x.key);
        RectHV otherHalf;

        if (distance < candidateDistance) {
            candidate = x.key;
            candidateDistance = distance;
        }
        if (xCoord > xmax) System.out.println("xCoord: " + xCoord + " > xmax: " + xmax);
        if (yCoord > ymax) System.out.println("yCoord: " + yCoord + " > ymax: " + ymax);
        // move towards the point
        if (x.coord) {
            if (p.x() <= xCoord) {
                // even if this is false it is still a viable option if distance > subTreeDistance
                // move left
                candidate = distanceCheck(p, x.left, xmin, ymin, xCoord, ymax, candidate, candidateDistance);
                otherHalf = new RectHV(xCoord, ymin, xmax, ymax);
            } else {
                // even if this is false it is still a viable option if distance > subTreeDistance
                // move right
                candidate = distanceCheck(p, x.right, xCoord, ymin, xmax, ymax, candidate, candidateDistance);
                otherHalf = new RectHV(xmin, ymin, xCoord, ymax);
            }
        } else {
            if (p.y() <= yCoord) {
                // even if this is false it is still a viable option if distance > subTreeDistance
                //move down
                candidate = distanceCheck(p, x.left, xmin, ymin, xmax, yCoord, candidate, candidateDistance);
                otherHalf = new RectHV(xmin, yCoord, xmax, ymax);
            } else {
                // even if this is false it is still a viable option if distance > subTreeDistance
                // move up
                candidate = distanceCheck(p, x.right, xmin, yCoord, xmax, ymax, candidate, candidateDistance);
                otherHalf = new RectHV(xmin, ymin, xmax, yCoord);
            }
        }

        candidateDistance = candidate.distanceTo(p);
        double subTreeDistance = otherHalf.distanceTo(p);

        // move in the opposite direction of point
        if (candidateDistance > subTreeDistance) {
            // we continue with the other subTree because we found nothing better in the closer subTree
            if (x.coord) {
                if (p.x() <= xCoord) {
                    candidate = distanceCheck(p, x.right, xCoord, ymin, xmax, ymax, candidate, candidateDistance);
                } else {
                    // move left
                    candidate = distanceCheck(p, x.left, xmin, ymin, xCoord, ymax, candidate, candidateDistance);
                }
            } else {
                if (p.y() <= yCoord) {
                    // move up
                    candidate = distanceCheck(p, x.right, xmin, yCoord, xmax, ymax, candidate, candidateDistance);
                } else {
                    // move down
                    candidate = distanceCheck(p, x.left, xmin, ymin, xmax, yCoord, candidate, candidateDistance);
                }
            }
        }

        return candidate;
    }

    private Point2D pruneNearest(Point2D p, Node x, double xmin, double ymin, double xmax, double ymax) {

        if (x == null) return null;

        Point2D candidate = x.key;
        double distance;
        distance = p.distanceTo(x.key);
        candidate = distanceCheck(p, root, xmin, ymin, xmax, ymax, candidate, distance);

        return candidate;
    }

    public Point2D nearest(Point2D p) {

        checkNull(p);
        Point2D candidate;
        candidate = pruneNearest(p, root, 0.0, 0.0, 1.0, 1.0);
        return candidate;
    }



    private void pointsInsideRect(Node x, RectHV rect, LinkedList<Point2D> points, double xmin,
                                                 double ymin, double xmax, double ymax) {

        if (x == null) return;
        RectHV rectNode = new RectHV(xmin, ymin, xmax, ymax);
        if (rect.intersects(rectNode)) {
            if (rect.contains(x.key)) points.add(x.key);
            // we have a vertical line
            if (x.coord) {
                pointsInsideRect(x.right, rect, points, x.key.x(), ymin, xmax, ymax);
                pointsInsideRect(x.left, rect, points, xmin, ymin, x.key.x(), ymax);

            } else {
                pointsInsideRect(x.right, rect, points, xmin, x.key.y(), xmax, ymax);
                pointsInsideRect(x.left, rect, points, xmin, ymin, xmax, x.key.y());
            }
        }
    }

    // all points that are inside the rectangle (or on the boundary)
    public Iterable<Point2D> range(RectHV rect) {

        checkNull(rect);
        LinkedList<Point2D> points = new LinkedList<>();
        pointsInsideRect(root, rect, points, 0.0, 0.0, 1.0, 1.0);

        return points;
    }

    public static void main(String[] args) {
        boolean test = true;

        System.out.println("Test: " + test + " \nNegateTest: " + !test);
        KdTree t = new KdTree();
        System.out.println("Is empty: " + t.isEmpty());

        t.insert(new Point2D(0.625, 0.625));
        t.insert(new Point2D(0.25, 0.75));
        t.insert(new Point2D(1.0, 0.5));
        t.insert(new Point2D(0.5, 0.125));
        t.insert(new Point2D(0.75, 1.0));

//        t.insert(new Point2D(1.0, 0.0));
//        t.insert(new Point2D(0.875, 1.0));
//        t.insert(new Point2D(0.25, 0.875));
//        t.insert(new Point2D(0.125, 0.625));
//        t.insert(new Point2D(0.0, 0.375));

//        t.insert(new Point2D(0.7, 0.2));
//        t.insert(new Point2D(0.5, 0.4));
//        t.insert(new Point2D(0.2, 0.3));
//        t.insert(new Point2D(0.4, 0.7));
//        t.insert(new Point2D(0.9, 0.6));

//        t.insert(new Point2D(0.25, 0.625));
//        t.insert(new Point2D(0.375, 0.6875));
//        t.insert(new Point2D(0.0, 0.75));
//        t.insert(new Point2D(0.6875, 0.875));
//        t.insert(new Point2D(0.4375, 0.3125));
//        t.insert(new Point2D(0.5625, 0.375));
//        t.insert(new Point2D(0.0625, 0.5625));
//        t.insert(new Point2D(0.125, 0.25));
//        t.insert(new Point2D(0.5, 0.8125));
//        t.insert(new Point2D(0.8125, 0.5)); // point J

//        t.insert(new Point2D(0.875, 0.125));
//        t.insert(new Point2D(0.5, 1.0));
//        t.insert(new Point2D(0.75, 0.875));
//        t.insert(new Point2D(1.0, 1.0));
//        t.insert(new Point2D(0.375, 0.125));
//        t.insert(new Point2D(0.5, 0.25));
//        t.insert(new Point2D(0.125, 0.375));
//        t.insert(new Point2D(0.375, 0.75));
//        t.insert(new Point2D(0.75, 0.0));
//        t.insert(new Point2D(0.875, 0.5));
//        t.insert(new Point2D(0.875, 0.25));
//        t.insert(new Point2D(0.375, 0.0));
//        t.insert(new Point2D(1.0, 0.625));
//        t.insert(new Point2D(0.0, 0.5));
//        t.insert(new Point2D(0.0, 0.125));
//        t.insert(new Point2D(0.5, 0.375)); // Point P
//        t.insert(new Point2D(0.25, 0.0));
//        t.insert(new Point2D(0.625, 0.375));
//        t.insert(new Point2D(0.875, 1.0));
//        t.insert(new Point2D(0.625, 0.5));


//        t.insert(new Point2D(0.2, 0.5));
//        t.insert(new Point2D(0.45, 0.4));
//        t.insert(new Point2D(0.6, 0.4));
//        t.insert(new Point2D(0.8, 0.7));
//        t.insert(new Point2D(0.9, 0.8));
//        System.out.println("Size after 6 pcts: " + t.size() + "size of root: " + t.size(t.root));
//        t.insert(new Point2D(0.85, 0.5));
//        t.insert(new Point2D(0.65, 0.65));
//        t.draw();
//        System.out.println(t.size());
//        System.out.println(t.contains(new Point2D(0.9, 0.8)));
//        System.out.println(t.contains(new Point2D(0.85, 0.5)));
//        RectHV rect = new RectHV(0.1, 0.2, 0.95, 0.95);
        t.draw();
        RectHV rect = new RectHV(0.1875, 0.4375, 1.0, 1.0);
        StdDraw.setPenRadius(0.004);
        StdDraw.setPenColor(StdDraw.GREEN);
        rect.draw();
        for (Point2D p: t.range(rect)) {
            System.out.println(p);
        }

        Point2D c = new Point2D(0.125, 0.0);
        StdDraw.setPenColor(StdDraw.ORANGE);
        StdDraw.setPenRadius(0.05);
        StdDraw.point(c.x(), c.y());
        Point2D closest = t.nearest(c);
        System.out.println("Closest: " + closest);
        StdDraw.setPenRadius(0.005);
        StdDraw.setPenColor(StdDraw.MAGENTA);
        StdDraw.line(c.x(), c.y(), closest.x(), closest.y());
    }

}
