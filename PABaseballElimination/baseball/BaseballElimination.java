import edu.princeton.cs.algs4.FlowEdge;
import edu.princeton.cs.algs4.FlowNetwork;
import edu.princeton.cs.algs4.FordFulkerson;
import edu.princeton.cs.algs4.In;

import java.util.Map;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Arrays;
import java.util.List;
import java.util.LinkedList;

public class BaseballElimination {

    private Map<String, Integer> teamsIndex;
    private int nrTeams;
    private int[] wins, losses, remainingGames;
    private int[][] gamesBetweenTeams;
    private String[] teamNames;
    private final HashMap<String, List<String>> cache;

    // create a baseball division from given filename in format specified below
    public BaseballElimination(String filename) {
        String[][] fileLines = readFileLines(filename);
        BaseballElimination(fileLines);
        cache = new HashMap<String, List<String>>(numberOfTeams());
    }

    private List<String> getResult(String teamName) {
        int teamIndex = teamsIndex.get(teamName);
        if (!cache.containsKey(teamName)) {
            cache.put(teamName, checkCertificateOfElimination(teamIndex));
        }
        return cache.get(teamName);
    }

    private List<String> checkCertificateOfElimination(int teamIndex){
        List<String> certificateOfElimination = getUnreachableTeam(teamIndex);
        if (!certificateOfElimination.isEmpty()) {
            return certificateOfElimination;
        }
        certificateOfElimination = getInCutCertificate(teamIndex);

        return certificateOfElimination;
    }

    private List<String> getInCutCertificate(int teamIndex) {

        List<String> certificateOfElimination = new LinkedList<>();
        FlowEdge flowEdge;
        FordFulkerson ff;
       // first numbers of Vertexes are reserved for the teams
        // the rest are game vertices corresponding to the map
        // last 2 numbers are source and sink(destination)
        // build remaining games vertices map
        HashMap<Integer, Integer[]> vertexGamesMap1 = buildVertexGamesMap(teamIndex);
        final int vertices = vertexGamesMap1.size() + nrTeams + 1;
        int possibleWinsCapacity;
        final int maxWins = wins[teamIndex] + remainingGames[teamIndex];
        final int sourceVertex = vertices - 2, sinkVertex = vertices - 1;
        int sourceVertexMaxFlow = 0;
        FlowNetwork flowNetwork = new FlowNetwork(vertices);
        // build team vertices index map
        Map<Integer, Integer> teamsIndexMap = buildTeamsIndexMap(teamIndex);

        // from source we take info from vertexGamesMap to build FlowEdges
        // from each games vertices two FlowEdges to the corresponding teams with INFINITE capacity
        for (Map.Entry<Integer, Integer[]> entry : vertexGamesMap1.entrySet()) {
            int ss = gamesBetweenTeams[entry.getValue()[0]][entry.getValue()[1]];
            sourceVertexMaxFlow += ss;
            flowNetwork.addEdge(new FlowEdge(
                    sourceVertex,
                    entry.getKey(),
                    ss));
            flowNetwork.addEdge(new FlowEdge(entry.getKey(), teamsIndexMap.get(entry.getValue()[0]),
                    Double.POSITIVE_INFINITY));
            flowNetwork.addEdge(new FlowEdge(entry.getKey(), teamsIndexMap.get(entry.getValue()[1]),
                    Double.POSITIVE_INFINITY));
        }

        // key is Team index and value is team vertex index
        for (Map.Entry<Integer, Integer> teamEntries : teamsIndexMap.entrySet()) {
            possibleWinsCapacity = maxWins - wins[teamEntries.getKey()];
            flowNetwork.addEdge(new FlowEdge(teamEntries.getValue(), sinkVertex, possibleWinsCapacity));
        }
        ff = new FordFulkerson(flowNetwork, sourceVertex, sinkVertex);

        for (Map.Entry<Integer, Integer> teamEntries : teamsIndexMap.entrySet()) {
            if (ff.inCut(teamEntries.getValue())) certificateOfElimination.add(teamNames[teamEntries.getKey()]);
        }

        return certificateOfElimination;
    }

    private List<String> getUnreachableTeam(int teamIndex) {
        final List<String> certificateOfElimination = new LinkedList<String>();
        final int maxWins = wins[teamIndex] + remainingGames[teamIndex];
        for (int i = 0; i < nrTeams; i++) {
            if (maxWins < wins[i]) {
                certificateOfElimination.add(teamNames[i]);
                break;
            }
        }
        return certificateOfElimination;
    }

    // map key team index and value vertices index it gets
    private Map<Integer, Integer> buildTeamsIndexMap(int withOutTeam) {
        int vertexIndex = 0;
        Map<Integer, Integer> teamsIndexMap = new HashMap<>();
        for (int i = 0; i < nrTeams; i ++) {
            if (i == withOutTeam) continue;
            teamsIndexMap.put(i, vertexIndex++);
        }
        return teamsIndexMap;
    }

    private HashMap<Integer, Integer[]> buildVertexGamesMap(int withOutTeam) {

        int gamesVertexCount = nrTeams - 1;
        Integer[] teamGamesPair;
        HashMap<Integer, Integer[]> vertexGamesMap = new HashMap<>();

        for (int i = 0; i < gamesBetweenTeams.length; i++){
            if (i == withOutTeam) continue;

            for (int j = i; j < gamesBetweenTeams[i].length; j++) {
                if (j == withOutTeam) continue;

                if (gamesBetweenTeams[i][j] > 0) {
                    teamGamesPair = new Integer[]{i, j};
                    vertexGamesMap.put(gamesVertexCount++, teamGamesPair);
                }
            }
        }
        return vertexGamesMap;

    }

    /**
     * first line number of teams
     * second line column headers
     * next lines:
     *  * first column team names
     *  * 2nd column wins: w[i]
     *  * 3rd column losses: l[i]
     *  * 4th column remaining games: r[i]
     */
    private void BaseballElimination(String[][] fileLines) {

        int i, j, p, c;  // p tracks the team index

        this.nrTeams = Integer.parseInt(fileLines[0][0]);
        this.wins = new int[nrTeams];
        this.losses = new int[nrTeams]; remainingGames = new int[nrTeams];
        this.gamesBetweenTeams = new int[nrTeams][nrTeams];
        this.teamsIndex = new HashMap<>();
        this.teamNames = new String[nrTeams];

        for (i = 1, p = 0; i < fileLines.length; i++, p++) {
            j =0;
            teamsIndex.put(fileLines[i][j], p);
            teamNames[p] = fileLines[i][j++];
            wins[p] = Integer.parseInt(fileLines[i][j++]);
            losses[p] = Integer.parseInt(fileLines[i][j++]);
            remainingGames[p] = Integer.parseInt(fileLines[i][j++]);

            for (c = 0; j < fileLines[i].length; c++, j++) {
                gamesBetweenTeams[p][c] = Integer.parseInt(fileLines[i][j]);
            }
        }
    }

    private String[] stripLine(String fileLine) {
        ArrayList<String> strippedLine = new ArrayList<>();

        String[] splittedLine = fileLine.split(" ");

        for (int i = 0; i < splittedLine.length; i++) {
            if (splittedLine[i].isEmpty()) continue;
            else strippedLine.add(splittedLine[i]);
        }

        return strippedLine.toArray(new String[strippedLine.size()]);
    }

    private String[][] readFileLines(String filename) {
        In in = new In(filename);
        ArrayList<String[]> fileLines = new ArrayList<>();
        while (!in.isEmpty()) {
            fileLines.add(stripLine(in.readLine()));
        }

        return fileLines.toArray(new String[fileLines.size()][]);
    }

    public              int numberOfTeams()                        // number of teams
    {
        return nrTeams;
    }

    public              Iterable<String> teams()                                // all teams
    {
        return Arrays.asList(teamNames);
    }
    public              int wins(String team)                      // number of wins for given team
    {
        checkNull(team);
        checkTeamIsInDivision(team);
        return wins[teamsIndex.get(team)];
    }
    public              int losses(String team)                    // number of losses for given team
    {
        checkNull(team);
        checkTeamIsInDivision(team);
        return losses[teamsIndex.get(team)];
    }

    public              int remaining(String team)                 // number of remaining games for given team
    {
        checkNull(team);
        checkTeamIsInDivision(team);
        return remainingGames[teamsIndex.get(team)];
    }

    public              int against(String team1, String team2)    // number of remaining games between team1 and team2
    {
        checkNull(team1);
        checkNull(team2);
        checkTeamIsInDivision(team1);
        checkTeamIsInDivision(team2);
        return gamesBetweenTeams[teamsIndex.get(team1)][teamsIndex.get(team2)];
    }

    public          boolean isEliminated(String team)               // is given team eliminated?
    {
        checkNull(team);
        checkTeamIsInDivision(team);
        return !getResult(team).isEmpty();
    }

    public Iterable<String> certificateOfElimination(String team)  // subset R of teams that eliminates given team; null if not eliminated
    {
        checkNull(team);
        checkTeamIsInDivision(team);
        return !getResult(team).isEmpty() ? cache.get(team) : null;
    }

    private void checkNull(Object obj) {
        if (obj == null) throw new IllegalArgumentException("Calling function with null Argument!");
    }

    private void checkTeamIsInDivision(String team) {
        if (teamsIndex.get(team) == null ) throw new IllegalArgumentException("Calling function with a team that is not" +
                " part of the division");
    }

    public static void main(String[] args) {
//        String filePath = "/mnt/app/javaalgorithms/PABaseballElimination/baseball/teams4.txt";
        String filePath = "/mnt/app/javaalgorithms/PABaseballElimination/baseball/teams5.txt";
//        String filePath = "/mnt/app/javaalgorithms/PABaseballElimination/baseball/teams12.txt";
//        String filePath = "/mnt/app/javaalgorithms/PABaseballElimination/baseball/teams0.txt";

        BaseballElimination be = new BaseballElimination(filePath);
        String team = "Detroit";
        System.out.println("Team " + team + " is eliminated? " + be.isEliminated(team));
        System.out.println(be.certificateOfElimination(team));
//        System.out.println(be.certificateOfElimination("Chinaa"));
//        In in = new In(filePath);
//        while (!in.isEmpty()) {
//            String[] s = in.readLine().split(" ");
//            for (int i = 0; i < s.length; i++) {
//                if (s[i].isEmpty()){
//                    System.out.println(" " + new String(s[i].toCharArray()));
//                }
//                System.out.println(s[i]);
//            }
//
//        }



    }
}
