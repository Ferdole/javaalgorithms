package percolation;

public class PercolationMatrix {

    private int[][] matrix;
    // get bounds for matrix
    private int boundary;
    // creates n-by-n grid, with all sites initially blocked
    public PercolationMatrix(int n){
        boundary = n;
        if (n <= 0){
            throw new IllegalArgumentException("Value lower or equal to 0");
        }
        else{
//            int[][] matrix = new int[n][n];
            matrix = new int[n + 1][n + 1];
            for (int i=1; i<=n; i++){
                for (int j=1; j<=n; j++){
                    matrix[i][j] = 0;
                }
            }
        }
    }

    public String toString(){
        String strMatrix = "";
        int matrixLen = matrix.length - 1 ;
        for (int i=1; i <= matrixLen; i++){
            strMatrix = strMatrix + "\n";
            for (int j=1; j <= matrixLen; j++){
                strMatrix = strMatrix + "\t" + matrix[i][j];
            }
        }
        return strMatrix;
    }
    // opens the site (row, col) if it is not open already
    public void open(int row, int col){
        // once we open something we should check if it is in the top row and add it to the set of connected elements
        if (row > boundary ||  col > boundary || row <= 0 || col <= 0){
            throw new IllegalArgumentException("row/col values exceed boundaries !");
        }
        else {
            matrix[row][col] = 1;

//            if ( row >= boundary  || row <= 0 || col >= boundary || col <= 0){
//                throw new IllegalArgumentException("Row or Col out of boundary: " + boundary);
//            } else {
//                int elemenet = (row - 1) * boundary + (col - 1);
//            }
        }
    }

    // is the site (row, col) open?
    public boolean isOpen(int row, int col){
        if (row > boundary ||  col > boundary || row <= 0 || col <= 0){
            throw new IllegalArgumentException("row/col values exceed boundaries !");
        } else {
            if (matrix[row][col] == 1)
                return true;
            else return false;
        }
    }

    // is the site (row, col) full?
    public boolean isFull(int row, int col){

        return true;
    }

    // returns the number of open sites
    public int numberOfOpenSites() {
        int count = 0;
        for (int i = 1; i <= boundary; i++)
            for (int j = 1; j <= boundary; j++)
                if (matrix[i][j] == 1) count ++;
        return count;
    }

    // does the system percolate?
    public boolean percolates(){
        return true;
    }

    // test client (optional)
    public static void main(String[] args){
        System.out.println("HelloPercolation");
        PercolationMatrix x = new PercolationMatrix(2);
        System.out.println("X: " + x);
    }
}
