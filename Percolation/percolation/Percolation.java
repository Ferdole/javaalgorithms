package percolation;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {

    // consider this a matrix with every cell indexed eg for size 2:
    // 0 1
    // 2 3
    private boolean[][] matrix;
    private int top = 0;
    private int bottom;
    private int boundary;
    private WeightedQuickUnionUF wqf;
    private int openedSites = 0;
    // creates n-by-n grid, with all sites initially blocked
    public Percolation(int n){
        boundary = n;
        if (n <= 0) {
            throw new IllegalArgumentException("Argument should be greater than 0");
        } else {
            wqf = new WeightedQuickUnionUF(n * n + 2);
            matrix = new boolean[n][n];
            bottom = n * n + 1;
        }
    }

    private int getWQFindex(int i, int j){
        return boundary * (i - 1) + j;
    }
    // opens the site (row, col) if it is not open already
    public void open(int row, int col){

        if ( row > boundary  || row <= 0 || col > boundary || col <= 0){
            throw new IllegalArgumentException("Row or Col out of boundary: " + boundary);
        }
        else if (!isOpen(row, col)) {
            matrix[row - 1][col - 1] = true;
            // increment opened sites
            openedSites ++;
            // add element to union with top if on first row and bottom if on last row
            if (row == 1)
//                System.out.println("Connected: " + getWQFindex(row, col) + " with TOP");
                wqf.union(getWQFindex(row, col), top);
            if (row == boundary)
//                System.out.println("Connected: " + getWQFindex(row, col) + " with BOTTOM");
                wqf.union(getWQFindex(row, col), bottom);
            // check if neighbors are in set
            if (row > 1 && isOpen(row - 1, col)){
//                System.out.println("Connected: " + getWQFindex(row, col) + " with: " + getWQFindex(row - 1, col));
                wqf.union(getWQFindex(row, col), getWQFindex(row - 1, col));
            }
            if (row < boundary && isOpen(row + 1, col)){
//                System.out.println("Connected: " + getWQFindex(row, col) + " with: " + getWQFindex(row + 1, col));
                wqf.union(getWQFindex(row, col), getWQFindex(row + 1, col));
            }
            if (col > 1 && isOpen(row, col - 1)){
//                System.out.println("Connected: " + getWQFindex(row, col) + " with: " + getWQFindex(row, col - 1));
                wqf.union(getWQFindex(row, col), getWQFindex(row, col - 1));
            }
            if (col < boundary && isOpen(row, col + 1)){
//                System.out.println("Connected: " + getWQFindex(row, col) + " with: " + getWQFindex(row, col + 1));
                wqf.union(getWQFindex(row, col), getWQFindex(row, col + 1));
            }
        }
    }

    // is the site (row, col) open?
    public boolean isOpen(int row, int col){
        if (row > 0 && row <= boundary && col > 0 && col <= boundary)
            return matrix[row - 1][col - 1];
        else
            throw new IllegalArgumentException("Row/Col out of Bounds");
    }

    // is the site (row, col) full?
    public boolean isFull(int row, int col) {
        if (row > 0 && row <= boundary && col > 0 && col <= boundary)
            return wqf.find(top) == wqf.find(getWQFindex(row, col));
        else
            throw new IllegalArgumentException("Row/Col out of Bounds");
    }

    // returns the number of open sites
    public int numberOfOpenSites(){
        return openedSites;
    }

    // does the system percolate?
    public boolean percolates(){
        return wqf.find(top) == wqf.find(bottom);
    }

    // test client (optional)
    public static void main(String[] args){
//        Percolation p = new Percolation(4);
//        p.open(2,3);
//        System.out.println(p.isFull(2,3));
//        System.out.println(p.isOpen(2,3));
//        p.open(1,3);
//        p.open(2,1);
//        p.open(1,1);
//        System.out.println(p.isOpen(1,3));
//        System.out.println(p.isFull(1,3));
//        System.out.println(p.isFull(2,3));
//        System.out.println(p.isFull(2,1));
//        System.out.println(p.isOpen(2,1));
//        System.out.println(p.isFull(1,1));
//        Percolation p = new Percolation(2);
//        p.open(2,2);
//        System.out.println(p.isFull(2,2));
//        System.out.println(p.isOpen(2,2));
//        p.open(1,2);
//        p.open(1,1);
//        System.out.println(p.isFull(1,1));
//        System.out.println(p.isFull(1,1));
//        p.open(1,2);
//        System.out.println(p.isFull(1,2));
//        System.out.println(p.isOpen(1,1));
//        System.out.println(p.percolates());
        Percolation p = new Percolation(10);
        p.open(10,2);
        p.open(2,10);
        p.open(6,8);
        p.open(2,6);
        p.open(1,4);
        p.open(8,4);
        p.open(10,1);
        p.open(4,2);
        p.open(4,8);
        p.open(9,3);
        p.open(2,2);
        p.open(9,1);
        System.out.println(p.isFull(9,1));
        System.out.println(p.isOpen(9,1));
//        p.open(10,1);
//        System.out.println(p.isFull(10,1));
//        System.out.println(p.isOpen(10,1));
//        System.out.println(p.isFull(3,1));
//        System.out.println(p.isOpen(3,1));

    }

}
