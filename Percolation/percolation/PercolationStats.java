package percolation;

//import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {

    private int experimentCount;
    private Percolation p;
    private double[] fractions;
    private double stddev;
    private double meanVal;
    // perform independent trials on an n-by-n grid
    public PercolationStats(int n, int trials){

        if (n <= 0 || trials <= 0){
            throw new IllegalArgumentException("Give n < 0 and T < 0");
        }
        fractions = new double[trials];
        experimentCount = trials;
        for(int trial = 0; trial < trials; trial++){
            p = new Percolation(n);
            while(!p.percolates()){

                int row = StdRandom.uniform(1, n + 1);
                int col = StdRandom.uniform(1, n + 1);
                p.open(row, col);
            }
            double fraction = (double) p.numberOfOpenSites() / (n * n);
            fractions[trial] = fraction;
        }
//        stddev = stddev();
//        meanVal = mean();
    }

    // sample mean of percolation threshold
    public double mean(){
        return meanVal = StdStats.mean(fractions);

    }

    // sample standard deviation of percolation threshold
    public double stddev(){
//        System.out.println("STDDEV");
        return stddev = StdStats.stddev(fractions);
    }


    // low endpoint of 95% confidence interval
    public double confidenceLo(){
        return meanVal - (1.96 * stddev / Math.sqrt(experimentCount));
    }

    // high endpoint of 95% confidence interval
    public double confidenceHi(){
        return meanVal + (1.96 * stddev / Math.sqrt(experimentCount));
    }

    // test client (see below)
    public static void main(String[] args){
        int n = Integer.parseInt(args[0]);
        int T = Integer.parseInt(args[1]);
        PercolationStats ps = new PercolationStats(n, T);

//        String confidence = ps.confidenceLo() + ", " + ps.confidenceHi();
//        StdOut.println("mean                    = " + ps.mean());
//        StdOut.println("stddev                  = " + ps.stddev());
//        StdOut.println("95% confidence interval = " + confidence);

    }
}
