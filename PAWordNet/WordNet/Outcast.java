
public class Outcast {

    /**
     * WordNet Object
     * */
    private final WordNet wordnet;

    // constructor takes a WordNet object
    public Outcast(WordNet wordnet){
        Utils.checkNull(wordnet);

        this.wordnet = wordnet;
    }

    // given an array of WordNet nouns, return an outcast
    public String outcast(String[] nouns)   {
        Utils.checkNull(nouns);
        int maxDistance = -1;
        int currentDistance;
        String outcast = null;
        for (String noun : nouns){
            currentDistance = 0;
            for (int i = 0; i < nouns.length; i++) {
                currentDistance = currentDistance + wordnet.distance(noun, nouns[i]);
            }
            if (currentDistance > maxDistance) {
                maxDistance = currentDistance;
                outcast = noun;
            }
        }

        return outcast;
    }

    // see test client below
    public static void main(String[] args) {

    }
}
