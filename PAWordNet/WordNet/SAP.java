
import edu.princeton.cs.algs4.BreadthFirstDirectedPaths;
import edu.princeton.cs.algs4.Digraph;

public class SAP {

    private static class answerSAP {

        private int pathLength;
        private int vertex;

        public answerSAP(int pathLength, int vertex) {
            this.pathLength = pathLength;
            this.vertex = vertex;
        }

        public int getPathLength() {
            return pathLength;
        }

        public int getVertex() {
            return vertex;
        }
    }

    /**
     * Copy of Digraph used for queries.
     * */
    private final Digraph digraph;

    // constructor takes a digraph (not necessarily a DAG)
    public SAP(Digraph G) {
        this.digraph = new Digraph(G);
    }

    // length of shortest ancestral path between v and w; -1 if no such path
    public int length(int v, int w) {
        answerSAP answer = sap(v, w);
        return answer.getPathLength();
    }

    /**
     * Iterate through each vertex in the digraph an check if both BreadthFirstDirectedPaths have a path to that vertex
     * and what is the length
     * */
    private answerSAP sap(final int first, final int second) {
        BreadthFirstDirectedPaths bfs1 = new BreadthFirstDirectedPaths(this.digraph, first);
        BreadthFirstDirectedPaths bfs2 = new BreadthFirstDirectedPaths(this.digraph, second);

        return sapFinder(bfs1, bfs2);
    }

    private answerSAP sap(final Iterable<Integer> first, final Iterable<Integer> second) {
        BreadthFirstDirectedPaths bfs1 = new BreadthFirstDirectedPaths(this.digraph, first);
        BreadthFirstDirectedPaths bfs2 = new BreadthFirstDirectedPaths(this.digraph, second);

        return sapFinder(bfs1, bfs2);
    }

    private answerSAP sapFinder(final BreadthFirstDirectedPaths bfs1, final BreadthFirstDirectedPaths bfs2) {

        int previousLength = -1;
        int vertex = -1;
        for (int v = 0; v < digraph.V(); v ++) {
            if (bfs1.hasPathTo(v) && bfs2.hasPathTo(v)) {
                final int length = bfs1.distTo(v) + bfs2.distTo(v);
                if (previousLength == -1 || previousLength > length) {
                    previousLength = length;
                    vertex = v;
                }
            }
        }
        return new answerSAP(previousLength, vertex);
    }

    // a common ancestor of v and w that participates in a shortest ancestral path; -1 if no such path
    public int ancestor(int v, int w) {
        answerSAP answer = sap(v, w);
        return answer.getVertex();
    }

    // length of shortest ancestral path between any vertex in v and any vertex in w; -1 if no such path
    public int length(Iterable<Integer> v, Iterable<Integer> w) {
        Utils.checkNull(v);
        Utils.checkNull(w);
        if (Utils.emptyIterable(v) || Utils.emptyIterable(w))
            return -1;

        answerSAP answer = sap(v, w);
        return answer.getPathLength();
    }

    // a common ancestor that participates in shortest ancestral path; -1 if no such path
    public int ancestor(Iterable<Integer> v, Iterable<Integer> w) {
        Utils.checkNull(v);
        Utils.checkNull(w);
        if (Utils.emptyIterable(v) || Utils.emptyIterable(w))
            return -1;

        answerSAP answer = sap(v, w);
        return answer.getVertex();
    }

    // do unit testing of this class
    public static void main(String[] args){

    }

}
