
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.DirectedCycle;
import edu.princeton.cs.algs4.BreadthFirstDirectedPaths;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class WordNet {

    private Digraph wordNet;
    private HashMap<String, List<Integer>> nounsIndexes;
    private String[] synsetsArray;
    private SAP sapFinder;

    // constructor takes the name of the two input files
    public WordNet(String synsets, String hypernyms) {
        checkNull(synsets);
        checkNull(hypernyms);

        nounsIndexes = new HashMap<String, List<Integer>>();

        // read content of files
        synsetsArray = getSynsetsArray(synsets);
        ArrayList<String[]> hypernymsArray = getHypernymsArray(hypernyms);

        this.wordNet = new Digraph(synsetsArray.length);
        for (String [] hypernymRelations: hypernymsArray) {
            int originNode = Integer.parseInt(hypernymRelations[0]);
            for (int i = 1; i < hypernymRelations.length; i++) {
                int relatedNode = Integer.parseInt(hypernymRelations[i]);
                // from hypernymRelations first is the index, rest are the relations
                wordNet.addEdge(originNode, relatedNode);
            }
        }

        // check if Digraph is according to requirements:
        // 1. Is a DAG
        // 2. Is a rooted DAG
        if (new DirectedCycle(wordNet).hasCycle()) {
            throw new IllegalArgumentException("Provided Graph is not a rooted DAG");
        }
        Utils.checkRootedDigraph(wordNet);

        this.sapFinder = new SAP(wordNet);
//        System.out.println("Initialized Digraph");
    }

    /**
     * Read the hypernyms file
     * */
    private ArrayList<String[]> getHypernymsArray(String hypernymsFilePath) {
        ArrayList<String> hypernymsLines = readFile(hypernymsFilePath);
        ArrayList<String[]> hypernymsArray = new ArrayList<String[]>(hypernymsLines.size());
        for (String hypernymString : hypernymsLines)
            hypernymsArray.add(hypernymString.split(","));
        return hypernymsArray;
    }

    /**
     * Read Synsets file
     * */
    private String[] getSynsetsArray(String synsetsFilePath) {

        int synsetId;

        ArrayList<String> synsetLines = readFile(synsetsFilePath);
        String[] synsetNodes = new String[synsetLines.size()];
        for (String synsetLine : synsetLines) {
            // [0] id // [1] is the synset. A synset is composed of one or more nouns
            // [2] gloss. Is not of interest for assignment
            String[] synsetSplit = synsetLine.split(",");
            String[] synset = getSynset(synsetSplit[1]);
            synsetId = Integer.parseInt(synsetSplit[0]);

            for (String noun : synset) {
                addNounsIndexes(noun, synsetId);
            }

            synsetNodes[synsetId] = synsetSplit[1];
        }

        return synsetNodes;
    }

    private void addNounsIndexes(final String noun, final Integer synsetId) {
//        List<Integer> indexes = nounsIndexes.computeIfAbsent(noun, f -> new ArrayList<Integer>());
        List<Integer> indexes = nounsIndexes.get(noun);
        if (indexes == null) {
            indexes = new ArrayList<Integer>();
            nounsIndexes.put(noun, indexes);
        }
        indexes.add(synsetId);
    }

    private String[] getSynset(String strSynset) {
        return strSynset.split(" ");
    }

    /**
     * Read line of file
     * */
    private ArrayList<String> readFile(String filePath) {
        // read file
        In in = new In(filePath);
        ArrayList<String> fileLines = new ArrayList<>();
        while (!in.isEmpty()) {
            fileLines.add(in.readLine());
        }
        return fileLines;
    }

    private void checkNull(Object obj) {
        if (obj == null) throw new IllegalArgumentException("Calling function with null Argument!");
    }

    private void checkNoun(String noun) {
        if (!this.nounsIndexes.containsKey(noun)) throw new IllegalArgumentException("Noun not in Graph nouns");
    }

    // returns all WordNet nouns
    public Iterable<String> nouns() {
        // return all nouns from the graph.
        return this.nounsIndexes.keySet();
    }

    // is the word a WordNet noun?
    public boolean isNoun(String word) {
        checkNull(word);

        return this.nounsIndexes.containsKey(word);
    }

    // distance between nounA and nounB (defined below)
    public int distance(String nounA, String nounB) {
        checkNull(nounA);
        checkNull(nounB);
        checkNoun(nounA);
        checkNoun(nounB);

        Iterable<Integer> nounAVertex = this.nounsIndexes.get(nounA);
        Iterable<Integer> nounBVertex = this.nounsIndexes.get(nounB);

        return sapFinder.length(nounAVertex, nounBVertex);
    }

    // a synset (second field of synsets.txt) that is the common ancestor of nounA and nounB
    // in a shortest ancestral path (defined below)
    public String sap(String nounA, String nounB) {
        checkNull(nounA);
        checkNull(nounB);
        checkNoun(nounA);
        checkNoun(nounB);

        Iterable<Integer> nounAVertex = this.nounsIndexes.get(nounA);
        Iterable<Integer> nounBVertex = this.nounsIndexes.get(nounB);

        return synsetsArray[sapFinder.ancestor(nounAVertex, nounBVertex)];
    }

    // do unit testing of this class
    public static void main(String[] args) {

        Iterable<Integer> d = new ArrayList<Integer>();

        System.out.println(Utils.emptyIterable(d));

        Digraph dg = new Digraph(5);
        dg.addEdge(1, 0);
        dg.addEdge(1, 2);
        dg.addEdge(2, 3);
        dg.addEdge(3, 0);
        dg.addEdge(0, 4);
//        dg.addEdge(4, 0);

        Iterable<Integer> ar = Arrays.asList(1, 2);
        BreadthFirstDirectedPaths bfdp = new BreadthFirstDirectedPaths(dg, ar);

        System.out.println(bfdp);

//        WordNet wn = new WordNet("/mnt/app/javaalgorithms/PAWordNet/WordNet/synsets.txt", "/mnt/app/javaalgorithms/PAWordNet/WordNet/hypernyms.txt");
//
//        String s = "12, 34,12";
//        for (String z : s.split(","))
//            System.out.println("Int: " + Integer.parseInt(z.trim()));
//
//        In in = new In("/mnt/app/javaalgorithms/PAWordNet/WordNet/synsets.txt");
//
//        System.out.println("Reading file: " + in.getClass().getName());
//
//        while(!in.isEmpty()) {
//            System.out.println(in.readLine());
//        }
//        System.out.println("Reading line: " + in.readLine());
//        System.out.println("Argument count: " + args.length);
//        SAP sap = new SAP(args);
//        sap.printArgs();
    }
}
