
import edu.princeton.cs.algs4.Digraph;

public class Utils {

    /**
     * Private constructor. Prevents from instancing.
     */
    private Utils() {
    }

    public static void checkNull(Object obj) {
        if (obj == null) throw new IllegalArgumentException("Calling function with null Argument!");
    }

    public static <T> boolean emptyIterable(Iterable<T> iter) {
        return !iter.iterator().hasNext();
    }

    /**
     * There should be a vertex that has no adjacent node. IE all directions will eventually lead towards this one
     */
    public static void checkRootedDigraph(Digraph digraph) {
        int sinkCount = 0;
        for ( int v = 0; v < digraph.V(); v++) {
            if (!digraph.adj(v).iterator().hasNext())
                sinkCount ++;
        }
        if (sinkCount != 1) throw new IllegalArgumentException("Digraph is not a rooted DAG");
    }

}
