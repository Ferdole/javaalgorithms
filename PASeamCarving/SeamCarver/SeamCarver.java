package SeamCarver;

import edu.princeton.cs.algs4.Picture;

import java.awt.Color;
import java.util.Scanner;

/**
 * There is a better solution to avoid near duplicating code, by transposing the image
 * */
public class SeamCarver {

    private Picture pic;

    /**
     * Matrix with energy of pixels. Is recalculated after each column/row is removed
     * */
    private double[][] pixelEnergy;

    // create a seam carver object based on the given picture
    public SeamCarver(Picture picture) {
        Utils.checkNull(picture);
        // create a copy of the picture
        this.pic = new Picture(picture);

        this.pixelEnergy = CalculateEnergy(picture);
//        printArray(pixelEnergy);

    }

    private Picture removeVerticalSeam(Picture pic, int[] shortestCarve) {
        Picture carvedPic = new Picture(pic.width() - 1, pic.height());

        for (int i = 0; i < carvedPic.height(); i++) {
            for (int j = 0, picJ = 0; j < carvedPic.width(); j++, picJ++) {
                // this cannot be at the margin of the picture
                if (shortestCarve[i] == j) {
                    picJ++;
                }
                carvedPic.set(j, i, pic.get(picJ, i));
            }
        }

//        printPicture(pic);
//        printPicture(carvedPic);
//        carvedPic.show();
        return carvedPic;
    }

    private Picture removeHorizontalSeam(Picture pic, int[] shortestCarve) {
        Picture carvedPic = new Picture(pic.width(), pic.height() - 1);

        for (int j = 0; j < carvedPic.width(); j++) {
            for (int i = 0, picI = 0; i < carvedPic.height(); i++, picI++) {
                // this cannot be at the margin of the picture
                if (shortestCarve[j] == i) {
                    picI++;
                }
                carvedPic.set(j, i, pic.get(j, picI));
            }
        }

//        printPicture(pic);
//        System.out.println();
//        printPicture(carvedPic);
//        carvedPic.show();
        return carvedPic;
    }

    /**
     * start from first row
     * skip second and last column
     * keep a matrix with bottom -> up energies calculated
     *   * rebuild the path from this matrix
     * save the shortest Edge.
     */
    private double[][] shortestVerticalPath(double[][] pictureEnergy) {

        int i = 0;

        double[][] shortestEnergy = new double[pictureEnergy.length][pictureEnergy[0].length];

        for (int j = 0; j < pictureEnergy[i].length; j++) {
            getVerticalCurrentLevelShortestPath(pictureEnergy, shortestEnergy, j, i);
        }

        int[] shortestPath = getVerticalPathFromShortestEnergy(shortestEnergy);
//        System.out.println();
//        for (i = 0; i < shortestPath.length; i ++) {
//            System.out.printf(" " + shortestPath[i]);
//        }
//        printArray(shortestEnergy);
//        printEdges(edges);
        return shortestEnergy;
    }

    /**
     * start from first column
     * keep a matrix with right -> left energies calculated
     *   * rebuild the path from this matrix
     * save the shortest Edge.
     */
    private double[][] shortestHorizontalPath(double[][] pictureEnergy) {

        int j = 0;

        double[][] shortestEnergy = new double[pictureEnergy.length][pictureEnergy[0].length];

        for (int i = 0; i < pictureEnergy.length; i++) {
            getHorizontalCurrentLevelShortestPath(pictureEnergy, shortestEnergy, j, i);
        }

//        printArray(shortestEnergy);
        return shortestEnergy;
    }


    private int[] getVerticalPathFromShortestEnergy(double[][] shortestEnergy) {

        int[] shortestPath;
        int startColumn = 0;
        double smallestEnergy = Double.POSITIVE_INFINITY;

        // can't start from first column
        for (int i=0, j=1; j < shortestEnergy[0].length - 1; j++) {
            if (smallestEnergy > shortestEnergy[i][j]) {
                startColumn = j;
                smallestEnergy = shortestEnergy[i][j];
            }
        }
        shortestPath = buildVerticalShortestPath(shortestEnergy, startColumn);
        return  shortestPath;
    }

    private int[] buildVerticalShortestPath(double[][] shortestEnergy, int column) {
        int[] shortestEnergyPath = new int[shortestEnergy.length];
        shortestEnergyPath[0] = column;
        for (int i = 1; i < shortestEnergy.length; i ++) {
            // S
            if (
                    column + 1 < shortestEnergy[i].length && column - 1 >= 0 &&
                    shortestEnergy[i][column] <= shortestEnergy[i][column + 1] &&
                    shortestEnergy[i][column] <= shortestEnergy[i][column - 1]
            )
                shortestEnergyPath[i] = column;
            // SE
            else if (
                    column + 1 < shortestEnergy[i].length &&
                    column - 1 >= 0 &&
                    shortestEnergy[i][column - 1] <= shortestEnergy[i][column + 1] &&
                    shortestEnergy[i][column - 1] < shortestEnergy[i][column]
            ) {
                shortestEnergyPath[i] = column - 1;
                column--;
            }
            // SW
            else if (
                        column + 1 < shortestEnergy[i].length &&
                        column - 1 >= 0 &&
                        shortestEnergy[i][column + 1] < shortestEnergy[i][column - 1] &&
                        shortestEnergy[i][column + 1] < shortestEnergy[i][column]
            ) {
                shortestEnergyPath[i] = column + 1;
                column++;
            }
        }

        return shortestEnergyPath;
    }

    private int[] buildHorizontalShortestPath(double[][] shortestEnergy, int row) {
        int[] shortestEnergyPath = new int[shortestEnergy[row].length];
        shortestEnergyPath[0] = row;
        for (int j = 1; j < shortestEnergy[row].length; j ++) {
            // E
            if (
                    row + 1 < shortestEnergy.length && row - 1 >= 0 &&
                    shortestEnergy[row][j] <= shortestEnergy[row + 1][j] &&
                    shortestEnergy[row][j] <= shortestEnergy[row - 1][j]
            )
                shortestEnergyPath[j] = row;
            // NE
            else if (
                        row + 1 < shortestEnergy.length &&
                        row - 1 >= 0 &&
                        shortestEnergy[row - 1][j] <= shortestEnergy[row + 1][j] &&
                        shortestEnergy[row - 1][j] < shortestEnergy[row][j]
            ) {
                shortestEnergyPath[j] = row - 1;
                row--;
            }
            // SE
            else if (
                        row + 1 < shortestEnergy.length &&
                        row - 1 >= 0 &&
                        shortestEnergy[row + 1][j] < shortestEnergy[row - 1][j] &&
                        shortestEnergy[row + 1][j] < shortestEnergy[row][j]
            ) {
                shortestEnergyPath[j] = row + 1;
                row++;
            }
        }

        return shortestEnergyPath;
    }

    private void getVerticalCurrentLevelShortestPath(double[][] pictureEnergy, double[][] shortestEnergy, int x, int y) {
        double S, SW, SE;
        // calculate energy
        // shouldn't be able to call function if out of bound
        // S
        if ( y + 1 < pictureEnergy.length)
            S = getVerticalSmallestEnergyPath(pictureEnergy, shortestEnergy, x, y + 1);
        else
            S = Double.POSITIVE_INFINITY;
        // SE
        if ( x + 1 < pictureEnergy[y].length && y + 1 < pictureEnergy.length)
            SW = getVerticalSmallestEnergyPath(pictureEnergy, shortestEnergy, x + 1, y + 1);
        else
            SW = Double.POSITIVE_INFINITY;
        // SW
        if ( x - 1 >= 0 && y + 1 < pictureEnergy.length)
            SE = getVerticalSmallestEnergyPath(pictureEnergy, shortestEnergy, x - 1, y + 1);
        else
            SE = Double.POSITIVE_INFINITY;

        // we need to know where we go from current Vertex
        if (S <= SW && S <= SE) {
            shortestEnergy[y][x] = pictureEnergy[y][x] + S;
        } else if (SW < S && SW <= SE) {
            shortestEnergy[y][x] = pictureEnergy[y][x] + SW;
        } else {
            shortestEnergy[y][x] = pictureEnergy[y][x] + SE;
        }
    }

    private void getHorizontalCurrentLevelShortestPath(double[][] pictureEnergy, double[][] shortestEnergy, int x, int y) {
        double E, NE, SE;
        // calculate energy
        // shouldn't be able to call function if out of bound
        // E
        if ( x + 1 < pictureEnergy[y].length)
            E = getHorizontalSmallestEnergyPath(pictureEnergy, shortestEnergy, x + 1, y);
        else
            E = Double.POSITIVE_INFINITY;
        // NE
        if ( x + 1 < pictureEnergy[y].length && y - 1 >= 0)
            NE = getHorizontalSmallestEnergyPath(pictureEnergy, shortestEnergy, x + 1, y - 1);
        else
            NE = Double.POSITIVE_INFINITY;
        // SE
        if ( x + 1 < pictureEnergy[y].length && y + 1 < pictureEnergy.length)
            SE = getHorizontalSmallestEnergyPath(pictureEnergy, shortestEnergy, x + 1, y + 1);
        else
            SE = Double.POSITIVE_INFINITY;

        // we need to know where we go from current Vertex
        if (E <= NE && E <= SE) {
            shortestEnergy[y][x] = pictureEnergy[y][x] + E;
        } else if (NE < E && NE <= SE) {
            shortestEnergy[y][x] = pictureEnergy[y][x] + NE;
        } else {
            shortestEnergy[y][x] = pictureEnergy[y][x] + SE;
        }
    }

    private double getHorizontalSmallestEnergyPath(double[][] pictureEnergy, double[][] shortestEnergy, int x, int y) {

        // x is column
        // y is row
        // we skip the the column row since they all are 1k

        if (x == pictureEnergy[y].length - 2) {
            // second to last row should return raw energy
            shortestEnergy[y][x] = pictureEnergy[y][x];
            return pictureEnergy[y][x];
        }
        else if (shortestEnergy[y][x] == 0.0) {

            getHorizontalCurrentLevelShortestPath(pictureEnergy, shortestEnergy, x, y);

            return shortestEnergy[y][x];
        }
        else
            return shortestEnergy[y][x];
    }


    private double getVerticalSmallestEnergyPath(double[][] pictureEnergy, double[][] shortestEnergy, int x, int y) {

        // x is column
        // y is row
        // we skip the the last column since they all are 1k

        if (y == pictureEnergy.length - 2) {
            // second to last row should return raw energy
            shortestEnergy[y][x] = pictureEnergy[y][x];
            return pictureEnergy[y][x];
        }
        else if (shortestEnergy[y][x] == 0.0) {

            getVerticalCurrentLevelShortestPath(pictureEnergy, shortestEnergy, x, y);

            return shortestEnergy[y][x];
        }
        else
            return shortestEnergy[y][x];
    }


    private void  printArray(double[][] ar) {
        for (int i = 0; i < ar.length; i++) {
            System.out.println("\n");
            for (int j = 0; j < ar[i].length; j++) {
                System.out.printf(" " + ar[i][j]);
            }
        }
    }

    private void printPicture(Picture pict) {
        for (int i = 0; i < pict.height(); i ++) {
            System.out.println();
            for (int j = 0; j < pict.width(); j++) {
                System.out.printf(" " + pict.getRGB(j, i));
            }
        }
    }

    private double[][] CalculateEnergy(Picture picture) {
        double[][] calculatePixelEnergy = new double[picture.height()][picture.width()];

        // y is considered row
        // x is considered columns
        for (int y = 0; y < picture.height(); y++) {
            for (int x = 0; x < picture.width(); x++) {
                // border pixel have an energy of 1000
                if (
                        y == 0 || y == picture.height() - 1 ||
                        x == 0 || x == picture.width() - 1
                ) {
                    calculatePixelEnergy[y][x] = 1000;
                    continue;
                }

                calculatePixelEnergy[y][x] = pixelEnergy(picture, x, y);
            }
        }
        return calculatePixelEnergy;
    }

    private double pixelEnergy(Picture picture, int x, int y) {

        double xGradient = xGradient(picture, x, y);
        double yGradient = yGradient(picture, x, y);

        return Math.sqrt(xGradient + yGradient);
    }

    private double xGradient(Picture picture, int x, int y) {
//        System.out.println("\nx: " + x + " y: " + y);
        Color x0C = picture.get(x - 1, y);
        Color x1C = picture.get(x + 1, y);

        return pixelDiff(x0C, x1C);
    }

    private double yGradient(Picture picture, int x, int y) {

        Color y0C = picture.get(x, y - 1);
        Color y1C = picture.get(x, y + 1);

        return pixelDiff(y0C, y1C);
    }

    private double pixelDiff(Color c1, Color c2) {
        double sqRed = Math.pow(c1.getRed() - c2.getRed(), 2);
        double sqGreen = Math.pow(c1.getGreen() - c2.getGreen(), 2);
        double sqBlue = Math.pow(c1.getBlue() - c2.getBlue(), 2);

        return sqRed + sqGreen + sqBlue;
    }
    // current picture
    public Picture picture() {
        return new Picture(pic);
    }

    // width of current picture
    public int width() {
        return pic.width();
    }

    // height of current picture
    public int height() {
        return pic.height();
    }

    // energy of pixel at column x and row y
    public double energy(int x, int y) {
        if (x < 0 || x > pic.width() - 1) throw new IllegalArgumentException("x coordinate out of bounds");
        if (y < 0 || y > pic.height() - 1) throw new IllegalArgumentException("y coordinate out of bounds");

        return pixelEnergy[y][x];
    }

    private int[] getHorizontalPathFromShortestEnergy(double[][] shortestEnergy) {
        int[] shortestPath;
        int startRow = 0;
        double smallestEnergy = Double.POSITIVE_INFINITY;

        // can't start from the first row
        for (int i=1, j=0; i < shortestEnergy.length - 1; i++) {
            if (smallestEnergy > shortestEnergy[i][j]) {
                startRow = i;
                smallestEnergy = shortestEnergy[i][j];
            }
        }
        shortestPath = buildHorizontalShortestPath(shortestEnergy, startRow);

//        printArray(shortestEnergy);
//        System.out.println();
//        for (int i = 0; i < shortestEnergy[0].length; i ++) {
//            System.out.printf(" " + shortestPath[i]);
//        }

        return  shortestPath;

    }

    // sequence of indices for horizontal seam
    public int[] findHorizontalSeam() {

        double[][] shortestEnergy= shortestHorizontalPath(pixelEnergy);
        int[] shortestPath = getHorizontalPathFromShortestEnergy(shortestEnergy);
        return shortestPath;
    }

    // sequence of indices for vertical seam
    public int[] findVerticalSeam() {

        double[][] shortestEnergy = shortestVerticalPath(pixelEnergy);

        int[] shortestPath = getVerticalPathFromShortestEnergy(shortestEnergy);
//        System.out.println();
//        for (int i = 0; i < shortestPath.length; i++) {
//            System.out.printf(" " + shortestPath[i]);
//        }
        return shortestPath;

    }

    private void checkVerticalSeam(int[] seam) {
        if (seam.length != pic.height()) throw new IllegalArgumentException("seam[] Vertical size is different of " +
                "picture height");
        for (int i = 0; i < pic.height(); i++) {
            if (seam[i] < 0 || seam[i] >= pic.width()) throw new IllegalArgumentException("Vertical values of seam " +
                    "are outside of picture width");
        }
    }

    private void checkHorizontalSeam(int[] seam) {
        if (seam.length != pic.width()) throw new IllegalArgumentException("seam[] Horizontal size is different of " +
                "picture width");
        for (int j = 0; j < pic.width(); j++) {
            if (seam[j] < 0 || seam[j] >= pic.height()) throw new IllegalArgumentException("Horizontal values of seam are outside " +
                    "of picture height");
        }

    }


    private void checkSeamCorrectness(int[] seam) {
        // the diff between values can't be greater than 1
        for (int i = 1; i < seam.length; i++) {
            if (Math.abs(seam[i-1] - seam[i]) > 1 || seam[i] < 0) throw new IllegalArgumentException("Difference between seam[] values " +
                    "are greater than 1");
        }
    }

    private void checkHeight(Picture picture) {
        if (picture.height() <= 1) throw new IllegalArgumentException("Image height too small to remove something from it");
    }

    private void checkWidth(Picture picture) {
        if (picture.width() <= 1) throw new IllegalArgumentException("Image width too small to remove something from it");
    }

    private void highlightVerticalSeam(int[] seam) {
        Utils.checkNull(seam);


        Picture highlightedPic = new Picture(pic);

        for (int i = 0; i < seam.length; i++) {
            highlightedPic.set(seam[i], i, Color.RED);
        }
//        highlightedPic.show();
    }

    // remove horizontal seam from current picture
    public void removeHorizontalSeam(int[] seam){
        Utils.checkNull(seam);
        checkSeamCorrectness(seam);
        checkHorizontalSeam(seam);
        checkHeight(this.pic);
        this.pic = removeHorizontalSeam(pic, seam);
        this.pixelEnergy = CalculateEnergy(this.pic);
    }

    // remove vertical seam from current picture
    public void removeVerticalSeam(int[] seam) {
        Utils.checkNull(seam);
        checkSeamCorrectness(seam);
        checkVerticalSeam(seam);
        checkWidth(this.pic);
        this.pic = removeVerticalSeam(pic, seam);
        this.pixelEnergy = CalculateEnergy(this.pic);
    }
//
    //  unit testing (optional)
    public static void main(String[] args) {
        String imagePath = "/mnt/app/javaalgorithms/PASeamCarving/SeamCarver/HJoceanSmall.png";
//        String imagePath = "/mnt/app/javaalgorithms/PASeamCarving/SeamCarver/6x5.png";
//        String imagePath = "/mnt/app/javaalgorithms/PASeamCarving/SeamCarver/3x4.png";
//        String imagePath = "/mnt/app/javaalgorithms/PASeamCarving/SeamCarver/cam1.jpg";
        System.out.printf("dorel %s", "dorel");
        System.out.printf("dorel");
        System.out.printf("dorel");
        Scanner o = new Scanner(System.in);
        Picture picc = new Picture(imagePath);
        SeamCarver sc = new SeamCarver(picc);
        sc.picture().show();
        int[] seam;
        int repeat;
        System.out.println("\nRemove Vertical seam how many times: ");
//        repeat = Integer.parseInt(o.nextLine());
//
//        for (int i = 0; i < repeat; i++) {
//            seam= sc.findVerticalSeam();
//            sc.highlightVerticalSeam(seam);
//            sc.removeVerticalSeam(seam);
//        }
//        sc.pic.show();
        System.out.println("Done vertical");
        int[] Hseam  = sc.findHorizontalSeam();
//        sc.removeHorizontalSeam(Hseam);
        System.out.println("Done");


    }

}
