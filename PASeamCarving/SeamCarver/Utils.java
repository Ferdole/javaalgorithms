package SeamCarver;

public class Utils {
    /**
     * Private constructor. Prevents from instancing.
     */
    private Utils() {
    }

    public static void checkNull(Object obj) {
        if (obj == null) throw new IllegalArgumentException("Calling function with null Argument!");
    }

}
