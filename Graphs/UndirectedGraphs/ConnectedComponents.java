package UndirectedGraphs;

public class ConnectedComponents {
    private boolean marked[];
    private int[] id;
    private int count;

    public ConnectedComponents(UndirectedGraph G) {
        marked = new boolean[G.getGraphOrder()];
        id = new int[G.getGraphOrder()];
        for (int v = 0; v < G.getGraphOrder(); v++) {
            if (!marked[v]) {
                dsf(G, v);
                count ++;
            }
        }
    }

    public int count() {
        return count;
    }

    public int id(int v) {
        return id[v];
    }

    private void dsf(UndirectedGraph G, int v) {
        marked[v] = true;
        id[v] = count;
        for (int w : G.adj(v)) {
            if (!marked[w])
                dsf(G, w);
        }
    }
}
