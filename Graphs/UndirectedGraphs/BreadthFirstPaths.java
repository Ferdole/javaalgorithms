package UndirectedGraphs;

import java.util.LinkedList;

public class BreadthFirstPaths {
    // usually "distanceTo" but for simplicity we use marked
    private boolean[] marked;
    private int[] edgeTo;
    private int s;

    public BreadthFirstPaths(UndirectedGraph G, int s) {
        this.marked = new boolean[G.getGraphOrder()];
        this.edgeTo = new int[G.getGraphOrder()];
        bsf(G, s);
    }

    private void bsf(UndirectedGraph G, int s) {
        LinkedList<Integer> q = new LinkedList<Integer>();
        q.addFirst(s);
        marked[s] = true;
        while (!q.isEmpty()) {
            int v = q.removeLast();
            for (int w : G.adj(v)) {
                if (!marked[w]) {
                    q.addFirst(w);
                    marked[w] = true;
                    edgeTo[w] = v;
                }
            }
        }


    }

}
