package Streams;

import lombok.Builder;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.Integer.parseInt;

public class streamsTest {

    private abstract class S1 {
        final int n = 1;
        public int x;

        public int nPlusTree() {
            return this.n + 3;
        }

        public abstract int nPlusSome(int nr);

    }

    private class implementS1 extends S1 {

        public int x;

        @Override
        public int nPlusSome(int nr) {
            return 0;
        }
    }

    @Builder
    private static class CollectStreamKV1 {
        private String k;
        private Integer valueList;

//        @Override
//        public String toString(){
//            return ""
//        }


        @Override
        public String toString() {
            return "CollectStreamKV1{" +
                    "k='" + k + '\'' +
                    ", valueList=" + valueList +
                    '}';
        }
    }


    private static class someStreams {


    }

    public static void main(String[] args) {
        List<String> myList =
                Arrays.asList("a1", "a2", "b1", "c2", "c1");

        myList
                .stream()
                .map( x -> {
                    String[] z = x.split("");
                    return new CollectStreamKV1(z[0], Integer.parseInt(z[1]));
                }
                )
                        .forEach(System.out::println);

        List<CollectStreamKV1> csKV1 = myList
                .stream()
                .map( x -> {
                            String[] z = x.split("");
                            return new CollectStreamKV1(z[0], Integer.parseInt(z[1]));
                        }
                )
                .collect(Collectors.toList());

        System.out.println(" " + Arrays.toString("a1".split("")));

//        myList
//                .stream()
//                .filter(s -> s.startsWith("c"))
//                .map(String::toUpperCase)
//                .sorted()
//                .forEach(System.out::println);
//
//
//        List<Integer> ints = Arrays.asList(1 ,2, 3, 4, 5, 6, 7, 7, 1, 2, 3, 3, 4, 5, 6);
//
//        Map<Integer, List<Integer>> result =
//                ints.stream().collect(Collectors.groupingBy( x-> x));
//
//        System.out.println(result.entrySet());
//
//        for (Map.Entry<Integer, List<Integer>> entry : result.entrySet()){
//            System.out.println(String.format("Key: %d + Value: %s", entry.getKey(), entry.getValue()));
//        }

//                .map( x -> x);
//        result.forEach( r -> System.out.println(r));
    }
}
